#include "resource_manager.hpp"
#include <vector>
#include <map>
#include <string_view>
#include <thread>
#include <condition_variable>
#include "task_pool.hpp"
#include <iostream>

namespace hs::resources
{
namespace
{
std::mutex RESOURCES_MUTEX;
std::condition_variable RESOURCES_READY_CONDITION;
bool RESOURCES_READY = false;

class ResourceCache
{
public:
   ResourceCache() = default;
   ~ResourceCache() = default;

   void Init()
   {
   }

   void Shutdown()
   {
       Flush();
   }

   void Push(std::unique_ptr<IResource> resource)
   {
       if(!resource)
           return;
       std::string name(resource->Name());
       m_resources[name] = std::shared_ptr<IResource>(std::move(resource));
   }

   void Erase(std::string_view name)
   {
       auto it = m_resources.find(std::string(name));
       if( it == m_resources.end())
           return;
       it->second->Release();
       m_resources.erase(it);
   }

   void PreloadResources(const std::vector<std::string_view> resource_names)
   {
       tasks::Pool pool;
       std::vector<std::future<void>> futures;
       for(auto name : resource_names)
       {
           auto resource = Find(name);
           if(resource != nullptr && !resource->IsLoaded())
              futures.push_back(pool.PushTask([resource]()
              {
                  std::cout << "Resource load thread " << std::this_thread::get_id() << std::endl;
                  resource->Load();
                  std::cout << "Resource load finished thread " << std::this_thread::get_id() << std::endl;
              }));
       }
       for(auto& f : futures)
           f.wait();
       {
           std::unique_lock<std::mutex> lock(RESOURCES_MUTEX);
           RESOURCES_READY = true;
           RESOURCES_READY_CONDITION.notify_all();
       }
   }

   std::shared_ptr<IResource> Find(std::string_view name)
   {
       auto it = m_resources.find(std::string(name));
       if(it != m_resources.end())
          return it->second;
       return nullptr;
   }

   void Flush()
   {
       for(auto& pair : m_resources)
       {
           pair.second->Release();
       }
       m_resources.clear();
   }

private:
   std::map<std::string, std::shared_ptr<IResource>> m_resources;
   std::vector<std::string> m_loaded_resource_names;
};
} // anonymous namespace

class ResourceManager::Impl
{
public:
   void Init()
   {
      m_cache.Init();
   }

   void Shutdown()
   {
      m_cache.Shutdown();
   }

   void RegisterResource(std::unique_ptr<IResource> resource)
   {
      std::unique_lock< std::mutex > lock( RESOURCES_MUTEX );
      m_cache.Push(std::move(resource));
   }

   void ReleaseResource(std::string_view name)
   {
       m_cache.Erase(name);
   }

   void Flush()
   {
       m_cache.Flush();
   }

   void Wait()
   {
       std::unique_lock<std::mutex> lock(RESOURCES_MUTEX);
       std::cout << "Resources wait thread " << std::this_thread::get_id() << std::endl;
       RESOURCES_READY_CONDITION.wait(lock, [](){ return RESOURCES_READY; });
       RESOURCES_READY = false;
   }

   void Load(std::vector<std::string_view> names)
   {
       m_cache.PreloadResources(std::move(names));
   }

   std::shared_ptr<IResource> ResourceByName(std::string_view name)
   {
      std::unique_lock<std::mutex> lock(RESOURCES_MUTEX);
      return m_cache.Find(name);
   }
private:
   ResourceCache m_cache;
};

void ResourceManager::RegisterResource(std::unique_ptr<IResource>&& resource)
{
    m_impl->RegisterResource(std::move(resource));
}

std::shared_ptr<IResource> ResourceManager::ResourceByName(std::string_view name)
{
    return m_impl->ResourceByName(name);
}

void ResourceManager::ReleaseResource(std::string_view name)
{
    m_impl->ReleaseResource(name);
}

void ResourceManager::Load(std::vector<std::string_view> names)
{
    m_impl->Load(std::move(names));
}

void ResourceManager::Flush()
{
    m_impl->Flush();
}

void ResourceManager::Init()
{
   m_impl->Init();
}

void ResourceManager::Shutdown()
{
    m_impl->Shutdown();
}

ResourceManager::ResourceManager() : m_impl(std::make_unique<Impl>())
{
}

void ResourceManager::Wait()
{
    m_impl->Wait();
}

ResourceManager::~ResourceManager()=default;
} // namespace
