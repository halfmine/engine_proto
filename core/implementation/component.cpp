#include "component.hpp"

namespace hs::core
{
void Component::Load()
{
    // TODO:
}

void Component::Update()
{
}

void Component::Destroy()
{
    resources::ResourceManager::Instance().ReleaseResource(m_resource_name);
}
} // namespace hs::core
