#include "resource_subsystem.hpp"
#include "resource_manager.hpp"

namespace hs::resources
{
void ResourceSubsystem::Init()
{
   ResourceManager::Instance().Init();
}

void ResourceSubsystem::Run()
{
}

void ResourceSubsystem::Shutdown()
{
   ResourceManager::Instance().Shutdown();
}
} // namespace hs::resources
