#include "input.hpp"

namespace hs::core
{
void Input::FirePressedEvent(Input::Key key)
{
    m_pressed_event(key);
}

void Input::FireReleasedEvent(Input::Key key)
{
    m_released_event(key);
}

} // namespace hs::core
