#include "app_layer.hpp"
#include "subsystem.hpp"
#include "resource_subsystem.hpp"
#include "events.hpp"
#include "graphics_subsystem.hpp"
#include "serialization.hpp"
#include "camera.hpp"
#include "input.hpp"

#include <memory>
#include <thread>
#include <map>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace hs::app_layer
{
static void FramebufferChangedCallback(GLFWwindow* /*window*/, int /*width*/, int /*height*/)
{
    // TODO
}

static std::map<int, core::Input::Key> INPUT_MAPPING = {{GLFW_KEY_ESCAPE, core::Input::Key::ESC},
                                                        {GLFW_KEY_UP, core::Input::Key::UP},
                                                        {GLFW_KEY_DOWN, core::Input::Key::DOWN},
                                                        {GLFW_KEY_LEFT, core::Input::Key::LEFT},
                                                        {GLFW_KEY_RIGHT, core::Input::Key::RIGHT},
                                                        {GLFW_KEY_W, core::Input::Key::W},
                                                        {GLFW_KEY_A, core::Input::Key::A},
                                                        {GLFW_KEY_S, core::Input::Key::S},
                                                        {GLFW_KEY_D, core::Input::Key::D},
                                                        {GLFW_KEY_Q, core::Input::Key::Q},
                                                        {GLFW_KEY_E, core::Input::Key::E}}; // TODO

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto& input = AppLayer::Instance().GetInput();
    if(action == GLFW_PRESS)
    {
        auto it = INPUT_MAPPING.find(key);
        if(it == INPUT_MAPPING.end())
            return;
        input.FirePressedEvent(it->second);
    }
    else if(action == GLFW_RELEASE)
    {
        auto it = INPUT_MAPPING.find(key);
        if(it == INPUT_MAPPING.end())
            return;
        input.FireReleasedEvent(it->second);
    }
}

class AppLayer::Impl
{
public:
   Impl() : m_window(nullptr)
   {
   }

   ~Impl() = default;

   void Init(const AppOptions& opts)
   {
      m_opts = opts;
      InitWindow();
      RegisterSubsystems();
      subsystems::SubsystemManager::Instance().Init();

      m_finished += subsystems::SubsystemManager::Instance().GetFinishedHandler();
      m_run += subsystems::SubsystemManager::Instance().GetRunHandler();
   }

   void Run()
   {
      m_run(); // Fire main Run event
      while(!glfwWindowShouldClose(m_window))
      {
         glfwSwapBuffers(m_window);
         glfwPollEvents();
      }
      m_finished();
   }

   void Shutdown()
   {
      subsystems::SubsystemManager::Instance().Shutdown();
   }

   core::Input& GetInput() { return m_input; }
private:
   GLFWwindow* m_window;


   void InitWindow()
   {
      glfwInit();

      glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
      m_window = glfwCreateWindow(m_opts.screen_width, m_opts.screen_height, m_opts.window_title.data(), nullptr, nullptr);

      glfwSetWindowUserPointer(m_window, this);
      glfwSetFramebufferSizeCallback(m_window, FramebufferChangedCallback);
      glfwSetKeyCallback(m_window, KeyCallback);
   }

   void RegisterSubsystems()
   {
      subsystems::SubsystemManager::Instance().Register(std::make_unique<resources::ResourceSubsystem>());
      subsystems::SubsystemManager::Instance().Register(std::make_unique<graphics::GraphicsSubsystem>(graphics::GraphicsEngineType::Vulkan, m_window, m_opts.scene_path));
   }

   AppOptions m_opts;
   core::Input m_input;
   events::Event<> m_finished;
   events::Event<> m_run;
};

AppLayer::AppLayer() : m_impl(std::make_unique<Impl>())
{
}

core::Input& AppLayer::GetInput()
{
    return m_impl->GetInput();
}

void AppLayer::Init(const AppOptions& opts)
{
   m_impl->Init(opts);
}

void AppLayer::Run()
{
   m_impl->Run();
}

void AppLayer::Shutdown()
{
   m_impl->Shutdown();
}

AppLayer::~AppLayer() = default;
} // namespace hs::app_layer
