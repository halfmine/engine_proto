#include "subsystem.hpp"
#include "app_exception.hpp"
#include <thread>
#include <future>
#include <vector>
#include <atomic>
#include <iostream> // TODO: logging

namespace hs::subsystems
{
namespace
{
std::atomic_bool FINISHED = false;
}

class SubsystemManager::Impl
{
public:
   Impl() = default;
   ~Impl() = default;
   
   void Register(std::unique_ptr<ISubsystem> subsystem_ptr)
   {
      m_subsystems_queue.push_back(std::move(subsystem_ptr));
   }

   void Init()
   {
      for(auto& subsystem : m_subsystems_queue)
      {
         subsystem->Init();
      }
   }
   
   void OnFinish()
   {
       FINISHED = true;
       for(auto& worker : m_workers)
           if(worker.valid())
               worker.get();
   }

   void OnRun()
   {
       Run();
   }

   void Run()
   {
        m_workers.resize(m_subsystems_queue.size());
        std::cout << "Subsystems: m_workers.size : " << m_workers.size() << "." << std::endl;
        for(auto& subsystem : m_subsystems_queue)
        {
            auto work = [&subsystem]()
            {
                try
                {
                    for(;;)
                    {
                        if(FINISHED)
                            break;
                        subsystem->Run();
                    }
                }
                catch(app_layer::AppException& app_exc)
                {
                    std::cerr << "Subsystems: Exception in worker thread: " << app_exc.what() << std::endl;
                }
                catch(...)
                {
                    std::cerr << "Subsystems: Exception in worker thread" << std::endl;
                }
            };

            m_workers.push_back(std::async(std::launch::async, work));
        }
   }

   void Shutdown()
   {
      for(auto it = m_subsystems_queue.rbegin(); it != m_subsystems_queue.rend(); ++it)
      {
         (*it)->Shutdown();
      }
   }
private:
   SubsystemManager::Queue m_subsystems_queue;
   std::vector<std::future<void>> m_workers;
};

void SubsystemManager::Register(std::unique_ptr<ISubsystem> subsystem_ptr)
{
   m_impl->Register(std::move(subsystem_ptr));
}

SubsystemManager::SubsystemManager() : m_impl(std::make_unique<SubsystemManager::Impl>()), 
                                       m_app_finished_handler([this]{ m_impl->OnFinish(); }),
                                       m_run_handler([this]{m_impl->OnRun(); })
{
}

void SubsystemManager::Init()
{
    m_impl->Init();
}

void SubsystemManager::Shutdown()
{
   m_impl->Shutdown();
}

SubsystemManager::~SubsystemManager()
{
}
} // namespace hs::subsystems
