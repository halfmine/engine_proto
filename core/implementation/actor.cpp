#include "actor.hpp"
#include "component.hpp"
#include <mutex>

namespace hs::core
{
namespace
{
std::mutex MODIFY_COMPONENTS_MTX;
}

Actor::Actor(hs::core::Actor::Id id) : m_id(id)
{
}

std::weak_ptr<Component> Actor::GetComponent(Actor::Id id)
{
    auto it = m_components_by_id.find(id);
    if(it == m_components_by_id.end())
        return std::weak_ptr<Component>();
    return it->second;
}

std::weak_ptr<Component> Actor::GetComponent(std::string_view name)
{
    auto it = m_components_by_name.find(std::string{ name.data(), name.size() });
    if(it == m_components_by_name.end())
        return std::weak_ptr<Component>();

    return it->second;
}

Actor::Id Actor::AddComponent(std::shared_ptr<Component> component)
{
    std::unique_lock< std::mutex > lock(MODIFY_COMPONENTS_MTX);
    m_components_by_name[std::string{ component->Name() }] = component;
    m_components_by_id[component->GetId()] = component;
    return component->GetId();
}

void Actor::Init()
{
    for(auto& component_pair : m_components_by_id)
    {
        auto component_ptr = component_pair.second;
        component_ptr->Load();
    }
}

void Actor::PostInit()
{
    // TODO:
}

void Actor::Update()
{
    for(auto& component_pair : m_components_by_id)
    {
        auto component_ptr = component_pair.second;
        component_ptr->Update();
    }
}

void Actor::Destroy()
{
    for(auto& component_pair : m_components_by_id)
    {
        auto component_ptr = component_pair.second;
        component_ptr->Destroy();
    }
}

Actor::~Actor() = default;

uint32_t ActorFactory::m_last_actor_id = 0;
uint32_t ActorFactory::m_last_component_id = 0;

std::shared_ptr<Actor> ActorFactory::Create()
{
    return std::make_shared<Actor>(++m_last_actor_id);
}

} // namespace hs::core
