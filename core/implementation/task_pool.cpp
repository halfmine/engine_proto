#include "task_pool.hpp"

namespace hs::tasks
{
namespace
{
std::mutex QUEUE_MUTEX;
}

Pool::Pool(size_t workers_count) : m_finished(false)
{
    // launch worker threads
    for(size_t i = 0; i < workers_count; i++)
        m_workers.push_back(std::async(std::launch::async, [this]{ WorkerThread(); }));
}

Pool::~Pool()
{
    m_finished = true;
    // Wait for workers to finish
    for(std::future<void>& worker : m_workers)
        worker.wait();
}

void Pool::WorkerThread()
{
    while(!m_finished)
    {
        std::unique_lock<std::mutex> lock(BlockingMutex());
        if(m_task_queue.empty())
            continue;
        std::packaged_task<void()> task;
        task = std::move(m_task_queue.front());
        m_task_queue.pop();
        if(task.valid())
            task();
    }
}

std::mutex& Pool::BlockingMutex()
{
    return QUEUE_MUTEX;
}
} // namespace hs::tasks
