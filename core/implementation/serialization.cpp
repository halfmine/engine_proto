#include "serialization.hpp"
#include <fstream>
#include <sstream>
#include <nlohmann/json.hpp>
#include "primitives.hpp"
#include "mesh.hpp"
#include "shader.hpp"
#include "actor.hpp"
#include "texture.hpp"
#include "scene_nodes.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace hs::core
{
void SceneSerializer::Serialize(std::string_view /*filename*/, const std::shared_ptr<graphics::Scene> /*scene*/)
{
    // TODO:
//        nlohmann::json j;
//        j["Scene"] = scene->Root()->Name();
//        for(auto child : scene->Root()->Children())
//        {
//            auto child_ptr = child.lock();
//            if(!child_ptr)
//                continue;
//        }
//        std::ifstream f(filename.data());
//        if(!f.is_open())
//        {
//            std::string msg = "Cannot serialize ";
//            msg += filename;
//            throw SerializerException(msg);
//        }
}

std::shared_ptr<graphics::Scene> SceneSerializer::Deserialize(std::string_view filename)
{
    std::shared_ptr< hs::graphics::Scene > scene = std::make_shared< hs::graphics::Scene >();
    nlohmann::json scene_json;
    std::ifstream in(std::string{filename.data(), filename.size()});
    if(!in.is_open())
        throw SerializerException(filename);
    try
    {
        in >> scene_json;
    }
    catch(nlohmann::detail::exception const& e)
    {
        throw SerializerException(e.what());
    }

    // TODO: deserizalize children
    auto actors = scene_json["actors"];
    auto cameras = scene_json["cameras"];
    auto lights = scene_json["lights"];
    auto resources = scene_json["resources"];
    auto shaders = scene_json["shaders"];

    std::vector<std::string_view> load_names;
    for(auto& resource_el : resources)
    {
        std::unique_ptr<resources::IResource> resource_ptr;
        if(resource_el["type"] == "Mesh")
        {
            resource_ptr.reset(new graphics::MeshResource(resource_el["filename"], resource_el["name"]));
        }
        else if(resource_el["type"] == "Shader")
        {
            resource_ptr.reset(new graphics::ShaderResource(resource_el["filename"], resource_el["name"]));
        }
        else if(resource_el["type"] == "Texture")
        {
            resource_ptr.reset(new graphics::TextureResource(resource_el["filename"], resource_el["name"]));
        }
        else
            throw SerializerException("Failed to read resources");
        load_names.push_back(resource_ptr->Name());
        resources::ResourceManager::Instance().RegisterResource(std::move(resource_ptr));
    }
    resources::ResourceManager::Instance().Load(load_names);
    resources::ResourceManager::Instance().Wait();

    for(auto& actor_el : actors)
    {
        std::shared_ptr<Actor> actor = ActorFactory::Create();
        actor->SetName(actor_el["name"]);
        for(auto& component : actor_el["components"])
        {
            auto component_name = component["name"];
            auto resource_name = component["resource_name"].get<std::string>();
            if(component["type"] == "Mesh")
            {
                auto matrix = component["initial_matrix"].get<std::vector<float>>();
                actor->SetIdentity(glm::make_mat4(matrix.data()));

                auto mesh_component = ActorFactory::CreateComponent<graphics::MeshComponent>(component_name, resource_name);
                actor->AddComponent(mesh_component);
            }
            else if(component["type"] == "Texture")
            {
                auto texture_component = ActorFactory::CreateComponent<core::Component>(component_name, resource_name);
                actor->AddComponent(texture_component);
            }
        }
        scene->AddNode(std::make_shared< graphics::ActorNode >(actor));
    }

    for(auto& shader : shaders)
    {
        graphics::ShaderNode::ShaderType type;
        if( shader["type"] == "fragment")
            type = graphics::ShaderNode::ShaderType::Fragment;
        else if( shader["type"] == "vertex")
            type = graphics::ShaderNode::ShaderType::Vertex;
        scene->AddNode(std::make_shared<graphics::ShaderNode>(shader["name"], shader["resource_name"], type));
    }
    scene->Init();
    return scene;
}

SceneSerializer::~SceneSerializer() = default;
} // namespace hs::core
