#pragma once
#include "events.hpp"

namespace hs::core
{
class Input
{
public:
    enum class Key
    {
        Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        ESC
    };

    Input() = default;

    void FirePressedEvent(Key key);
    void FireReleasedEvent(Key key);

    void AddListener(events::EventHandler<Key> pressed_listener, events::EventHandler<Key> released_listener)
    {
        m_pressed_event += pressed_listener;
        m_released_event += released_listener;
    }
private:
    events::Event<Key> m_pressed_event;
    events::Event<Key> m_released_event;
};
} // namespace hs::core
