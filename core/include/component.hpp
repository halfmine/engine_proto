#pragma once
#include "actor.hpp"
#include <resource_manager.hpp>
#include <string_view>
#include <vector>

namespace hs::core
{
class Component
{
public:
    friend class ActorFactory;

    Component(std::string name, std::string resource_name) : m_name(std::move(name)), m_resource_name(std::move(resource_name))
    {
    }

    virtual ~Component() = default;

    virtual void Load();
    virtual void Update();
    virtual void Destroy();

    inline Actor::Id GetId() noexcept { return m_id; }
    inline std::string_view Name() const noexcept { return m_name; }

    std::string ResourceName()
    {
        return m_resource_name;
    }

    std::shared_ptr<resources::IResource> Resource() const
    {
        return resources::ResourceManager::Instance().ResourceByName(m_resource_name);
    }
protected:
    Actor::Id m_id;
    std::string m_name;
    std::string m_resource_name;
};
} // namespace hs::core
