#pragma once
#include <exception>
#include <string>
#include <string_view>

namespace hs::app_layer
{
/// Basic app layer exception
class AppException : std::exception
{
public:
    AppException(const std::string msg) : msg(std::move(msg))
    {
    }

    AppException(const std::string_view msg) : msg(msg.data())
    {
    }

    AppException(const char* msg_chars) : msg(msg_chars)
    {
    }

    const char *what() const noexcept override
    {
        return msg.data();
    }
private:
    std::string msg;
};
} // namespace hs::app_layer
