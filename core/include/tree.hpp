#pragma once
#include <vector>
#include <memory>
#include <map>

namespace hs::core
{
// TODO:
template<typename NodeT>
class Tree
{
public:
    using DataPtr = std::shared_ptr<NodeT>;

    struct Node
    {
       DataPtr data;
       std::vector<Node*> children;
    };
    
    using NodePtr = std::shared_ptr<Node>;
    
    class Iterator
    {
    public:
        Iterator(Node* node) : m_current_node(node), m_visited_nodes({})
        {
        }
        
        void Next()
        {
            for(auto& child : m_current_node->children)
            {
                auto it = m_visited_nodes.find(child);
                if(it != m_visited_nodes.end())
                    continue;
            }
        }
        
        void operator++()
        {
            Next();
        }
        
        void operator+(size_t indexes)
        {
            for(size_t i = 0; i < indexes; i++)
                Next();
        }
        
        Node& operator*()
        {
            return *m_current_node;
        }
    private:
        Node* m_current_node;
        std::map<Node*, bool> m_visited_nodes;
    };

    Tree(NodePtr root) : m_root(root)
    {
    }
public:
    NodePtr m_root;
};
} // namespace hs::core
