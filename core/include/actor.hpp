#pragma once
#include <memory>
#include <string>
#include <string_view>
#include <map>
#include <utility>
#include <type_traits>
#include "math.hpp"

namespace hs::core
{
class Component;

class Actor
{
public:
    using Id = uint16_t;

    Actor(Id id);
    ~Actor();

    std::weak_ptr< Component > GetComponent(Id id);
    std::weak_ptr< Component > GetComponent(std::string_view name);
    Id AddComponent(std::shared_ptr< Component > component);

    void Init();
    void PostInit();
    void Update();
    void Destroy();
    std::string_view Name() { return m_name; }
    void SetName(std::string const& name) { m_name = name; }
    void SetIdentity(graphics::math::Mat4x4 mat) { m_identity_mat = mat; }
    graphics::math::Mat4x4 Identity() { return m_identity_mat; }
private:
    std::map<std::string, std::shared_ptr< Component > > m_components_by_name;
    std::map<Id, std::shared_ptr< Component > > m_components_by_id;
    std::string m_name;
    graphics::math::Mat4x4 m_identity_mat;
    Id m_id;
};

class ActorFactory
{
public:
    /// Create actor
    static std::shared_ptr<Actor> Create();

    /// Create Component
    template<typename ComponentT, typename... Params>
    static std::shared_ptr<std::enable_if_t<std::is_base_of_v<Component, ComponentT>, ComponentT>> CreateComponent(Params... p)
    {
        auto component_ptr = std::make_shared<ComponentT>(std::forward<Params>(p)...);
        component_ptr->m_id = ++m_last_component_id;
        return component_ptr;
    }
private:
    static uint32_t m_last_actor_id;
    static uint32_t m_last_component_id;
};
} // namespace hs::core
