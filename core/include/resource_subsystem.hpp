#pragma once
#include "subsystem.hpp"
#include <memory>

namespace hs::resources
{
class ResourceManager;

class ResourceSubsystem : public subsystems::ISubsystem
{
public:
    ResourceSubsystem() = default;
    ~ResourceSubsystem() override = default;

    void Init() override;
    void Run() override;
    void Shutdown() override;
};
} // namespace hs::resources
