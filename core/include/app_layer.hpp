#pragma once
#include <memory>
#include <string>
#include "input.hpp"

namespace hs::app_layer
{

struct AppOptions
{
   uint16_t screen_width;
   uint16_t screen_height;
   size_t msaa_count;
   std::string window_title;
   std::string scene_path;
};

class AppException;

/**
** \brief Singleton providing app layer entrypoint
**/
class AppLayer
{
public:
    ~AppLayer();

    // Non-copyable
    AppLayer(const AppLayer& rhs) = delete;
    void operator=(const AppLayer& rhs) = delete;
    
    // Non-movable
    AppLayer(AppLayer&& rhs) = delete;
    void operator=(AppLayer&& rhs) = delete;

    static AppLayer& Instance()
    {
        // most compilers provides thread-safe
        // static initialization for such locals
        static AppLayer inst;
        return inst;
    }

    core::Input& GetInput();
    void Init(const AppOptions& opts);
    void Run();
    void Shutdown();

private:
    // Default-constructible
    AppLayer();
    
    class Impl;
    std::unique_ptr<Impl> m_impl;
};
} // namespace hs::app_layer
