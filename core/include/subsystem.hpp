#pragma once

#include <vector>
#include <utility>
#include <memory>
#include "events.hpp"
#include "input.hpp"

namespace hs::subsystems
{
class ISubsystem
{
public:
    ISubsystem() = default;
    virtual ~ISubsystem() = default;

    virtual void Init()=0;
    virtual void Run()=0;
    virtual void Shutdown()=0;
};

class SubsystemManager
{
public:
    using Queue = std::vector<std::unique_ptr<ISubsystem>>;

    ~SubsystemManager();
    static SubsystemManager& Instance()
    {
        static SubsystemManager inst;
        return inst;
    }

    void Register(std::unique_ptr<ISubsystem> subsystem_ptr);
    void Init();
    void Shutdown();

    const events::EventHandler<>& GetFinishedHandler() const noexcept
    {
        return m_app_finished_handler;
    }
    
    const events::EventHandler<>& GetRunHandler() const noexcept
    {
        return m_run_handler;
    }

private:
    SubsystemManager();
    class Impl;
    std::unique_ptr<Impl> m_impl;

    events::EventHandler<> m_app_finished_handler;
    events::EventHandler<> m_run_handler;
};
} // namespace hs::subsystems
