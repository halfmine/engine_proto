#pragma once
#include <fstream>
#include <sstream>
#include <vector>
#include <memory>
#include <iostream> // TODO: logger

namespace hs::filesystem
{
namespace
{
void ErrorMessage(const std::string& msg)
{
    std::cout << msg << std::endl;
}
}

class FilesystemException : public std::exception
{
public:
    FilesystemException(const char* msg) : msg(msg)
    {
    }
    
    ~FilesystemException() override = default;
    const char * what() const noexcept override
    {
        return msg;
    }
private:
      const char* msg;
};

class File
{
public:
    File() : m_is(nullptr)
    {
    }

    File( std::string name )
    {
        m_name = std::move(name);
        Load();
    }

    void Load()
    {
        Load( m_name );
    }

    void Load(std::string_view name)
    {
        m_is = std::make_unique<std::ifstream>(name.data(), std::ios::ate | std::ios::binary);
        if(!m_is->is_open() || m_is->bad())
        {
            std::stringstream ss;
            ss << "Cannot open file " << name << std::endl;
            ErrorMessage(ss.str());
            throw FilesystemException(ss.str().c_str());
        }

        Clear();
        uint32_t pos = static_cast<uint32_t>(m_is->tellg());
        m_contents.resize(pos);
        m_is->seekg(0);
        m_is->read(m_contents.data(), pos);
        m_is->close();
    }
    
    void Close()
    {
        if(m_is->is_open())
            m_is->close();
    }
    
    ~File()
    {
        Close();
    }
    
    File(const File& other) = delete;
    File(File&& other)=default;

    const std::vector<char>& GetContents()
    {
        if(m_contents.empty())
        {
            Load();
        }

        return m_contents;
    }
    
    const char* GetRawContents()
    {
        return GetContents().data();
    }
    
    void Clear()
    {
        m_contents.clear();
    }

private:
    std::unique_ptr<std::ifstream> m_is;
    std::vector<char> m_contents;
    std::string m_name;
};
} // namespace filesystem
