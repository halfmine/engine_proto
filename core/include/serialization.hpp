#pragma once
#include <string_view>
#include <memory>

#include "scene.hpp"
#include "app_exception.hpp"

namespace hs::core
{

class SerializerException : public app_layer::AppException
{
public:
    SerializerException(const std::string msg) : app_layer::AppException(std::move(msg))
    {
    }

    SerializerException(const std::string_view msg) : app_layer::AppException(msg.data())
    {
    }

    SerializerException(const char* msg_chars) : app_layer::AppException(msg_chars)
    {
    }
};

class SceneSerializer
{
public:
    SceneSerializer() = default;
    ~SceneSerializer();

    void Serialize(std::string_view filename, const std::shared_ptr< hs::graphics::Scene > scene); // TODO
    std::shared_ptr< hs::graphics::Scene > Deserialize(std::string_view filename);
};
} // namespace hs::core
