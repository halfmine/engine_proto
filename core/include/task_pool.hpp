#pragma once
#include <future>
#include <memory>
#include <queue>
#include <atomic>
#include <list>
#include <algorithm>
#include <type_traits>

namespace hs::tasks
{

class Pool
{
public:
    Pool(size_t workers_count = 8);
    Pool(const Pool& rhs) = delete;
    Pool(Pool&& rhs) = delete;
    Pool& operator=(const Pool& rhs) = delete;
    Pool& operator=(Pool&& rhs) = delete;
    ~Pool();

    template<typename F>
    std::future<void> PushTask(F&& func)
    {
        std::packaged_task<void()> t(std::forward<F>(func));
        std::future<void> res = t.get_future();
        {
            std::unique_lock<std::mutex> lock(BlockingMutex());
            m_task_queue.push(std::move(t));
        }
        return res;
    }

    void WorkerThread();

private:
    std::mutex& BlockingMutex();
    std::queue<std::packaged_task<void()>> m_task_queue;
    std::list<std::future<void>> m_workers;
    std::atomic_bool m_finished;
};
} // namespace hs::tasks
