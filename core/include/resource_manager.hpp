#pragma once
#include <memory>
#include <string_view>
#include <string>
#include <exception>

namespace hs::resources
{

class ResourceException : public std::exception
{
public:
   ResourceException(const std::string& msg) : m_msg(msg)
   {
   }
   
   ResourceException(std::string_view msg) : m_msg(msg.data(), msg.size())
   {
   }

   ResourceException(const char* str) : m_msg(str)
   {
   }

   ~ResourceException () = default;

   const char*what() const noexcept
   {
      return m_msg.data();
   }
   
private:
   std::string m_msg;
};

/**
 * @brief Resources interface
 */
class IResource
{
public:
   virtual ~IResource() {}
   virtual void Load()=0;
   virtual const char* RawResource(size_t& out_resource_size)=0;
   virtual std::string_view Name()=0;
   virtual std::string_view FileName()=0;
   virtual void Release()=0;
   virtual bool IsLoaded() = 0;
};

/**
 * @brief Singleton for managing resources
 */
class ResourceManager
{
public:
   ~ResourceManager();

   static ResourceManager& Instance()
   {
      static ResourceManager inst;
      return inst;
   }

   void RegisterResource(std::unique_ptr<IResource>&& resource);
   std::shared_ptr<IResource> ResourceByName(std::string_view name);
   void ReleaseResource(std::string_view name);
   void Load(std::vector<std::string_view> names);
   void Flush();
   void Wait();

   // Loading all resources
   void Init(); // TODO
   void Shutdown();
private:
   ResourceManager();
   class Impl;
   std::unique_ptr<Impl> m_impl;
};
} // namespace hs::resources
