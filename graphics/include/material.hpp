#pragma once
#include "texture.hpp"

namespace hs::graphics
{
class Material
{
public:
    std::shared_ptr<TextureResource> m_texture = nullptr;
};
}
