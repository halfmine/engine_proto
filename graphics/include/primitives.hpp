#pragma once

#include "math.hpp"
#include <array>
#include <vector>
#include <glm/gtx/hash.hpp>
#include <glm/matrix.hpp>

namespace hs::graphics
{
struct Vertex
{
    Vertex() = default;
    ~Vertex() = default;
    Vertex(const Vertex&) = default;
    Vertex(Vertex&&) = default;
    Vertex& operator=(const Vertex&) = default;
    Vertex& operator=(Vertex&&) = default;

    bool operator==(Vertex const& rhs) const
    {
        return position == rhs.position
               && color == rhs.color
               && normal == normal;
    }

    math::Vec3 position;
    math::Vec3 normal;
    math::Vec3 color;
    math::Vec2 uv;
};

struct UniformBufferObject
{
   glm::mat4 model;
   glm::mat4 view;
   glm::mat4 proj;

   void Rotate(const float& duration, const float& radians)
   {
      model = glm::rotate(glm::mat4(1.0f), duration * glm::radians(radians), glm::vec3(0.0f, 0.0f, 1.0f));
   }

   void LookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up)
   {
      view = glm::lookAt(eye, center, up);
   }

   void PerspectiveProjection(const float& angle, const size_t& screen_width, const size_t& screen_height)
   {
      proj = glm::perspective(glm::radians(angle), screen_width / static_cast<float>(screen_height), 0.1f, 10.0f);
      proj[1][1] *= -1;
   }
};
} // namespace hs::graphics

namespace std
{
template<>
struct hash<hs::graphics::Vertex>
{
   size_t operator()(const hs::graphics::Vertex& vertex) const
   {
      return ((hash<glm::vec3>()(vertex.position) ^
             (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
             (hash<glm::vec3>()(vertex.normal) << 1);
   }
};
}
