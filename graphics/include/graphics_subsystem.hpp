#pragma once

#include "subsystem.hpp"
#include <memory>
#include "input.hpp"

namespace hs::graphics
{
enum class GraphicsEngineType
{
   Vulkan,
   DirectX12 // not used yet
};

class GraphicsSubsystem : public subsystems::ISubsystem
{
public:
    GraphicsSubsystem(const GraphicsEngineType type, void* window, std::string scene_path);
    ~GraphicsSubsystem();
    
    void Init() override;
    void Run() override;
    void Shutdown() override;
private:
    class Impl;
    std::unique_ptr<Impl> m_impl;
    GraphicsEngineType m_type;
};
} // namespace hs::graphics
