#pragma once
#include "resource_manager.hpp"
#include "component.hpp"
#include <memory>

namespace hs::graphics
{
class TextureResource : public resources::IResource
{
public:
    TextureResource(std::string filename, std::string name);
    ~TextureResource() override;
    void Load() override;
    const char* RawResource(size_t& out_resource_size) override;
    std::string_view Name() override;
    std::string_view FileName() override;
    void Release() override;
    bool IsLoaded() override;

    unsigned char const* ImageInfo(int& width, int& height, int& channels);
private:
    class Impl;
    std::unique_ptr< Impl > p_impl;
};

} // namespace hs::graphics
