#pragma once
#include <scene.hpp>
#include "events.hpp"
#include "math.hpp"
#include "texture.hpp"
#include "input.hpp"

#include <memory>

namespace hs::graphics
{
class IRenderer
{
public:
   virtual void Init()=0;
   virtual void Run()=0;
   virtual void Shutdown()=0;
   virtual const events::EventHandler<core::Input::Key>& InputHandler() = 0;
   virtual const events::EventHandler<core::Input::Key>& InputReleasedHandler() = 0;
   virtual ~IRenderer() {}
};
} // namespace hs::grpahics
