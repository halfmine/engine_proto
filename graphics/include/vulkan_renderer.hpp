#pragma once
#include "renderer.hpp"
#include "events.hpp"
#include <memory>
#include "app_exception.hpp"
#include "input.hpp"

namespace hs::graphics::vulkan
{
class VulkanRenderer : public IRenderer
{
public:
   VulkanRenderer(void* window);
   VulkanRenderer(const VulkanRenderer& rhs) = delete;
   VulkanRenderer(VulkanRenderer&& rhs) = default;
   void operator =(const VulkanRenderer& rhs) = delete;
   VulkanRenderer& operator =(VulkanRenderer&& rhs) = default;

   ~VulkanRenderer() override;

   void Init() override;
   void Run() override;
   void Shutdown() override;
   const events::EventHandler<core::Input::Key>& InputHandler() override;
   const events::EventHandler<core::Input::Key>& InputReleasedHandler() override;
private:
   class Impl;
   std::unique_ptr<Impl> m_impl;
   events::EventHandler<core::Input::Key> m_input_handler;
   events::EventHandler<core::Input::Key> m_input_release_handler;
};
} // namespace hs::graphics
