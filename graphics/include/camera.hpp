#pragma once
#include "scene.hpp"
#include "math.hpp"
#include <array>

namespace hs::graphics
{

struct Camera
{
    math::Mat4x4 m_view;
    math::Mat4x4 m_projection;
    math::Mat4x4 m_view_proj; // Position and orientation
};

class CameraNode : public AbstractSceneNode
{
public:
    CameraNode() = default;
    ~CameraNode() = default;

    std::string Name() override { return "camera"; }
    Type GetType() override { return Type::Camera; }
    std::string Stringify() override { return std::string{}; } // TODO
private:
    Camera m_camera;
};
}
