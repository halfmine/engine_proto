#pragma once
#include <memory>
#include <vector>
#include <map>
#include <unordered_map>
#include "actor.hpp"
#include "component.hpp"


namespace hs::graphics
{
class AbstractSceneNode
{
public:
    enum class Type
    {
        Camera,
        Light,
        Actor,
        Shader
    };

    AbstractSceneNode(std::shared_ptr<AbstractSceneNode> parent = nullptr) : m_parent(parent)
    {
    }

    virtual ~AbstractSceneNode() {}


    virtual void Init()
    {
        for( auto& child : m_children )
        {
            if(child)
               child->Init();
        }
    }

    virtual void Shutdown()
    {
        for(auto& child : m_children)
            if(child)
                child->Shutdown();
        m_children.clear();
    }

    virtual void OnUpdate()
    {
        for(auto& child : m_children)
            if(child)
                child->OnUpdate();
    }

    void AddChild(std::shared_ptr<AbstractSceneNode> child)
    {
        m_children.push_back( child );
    }
    std::vector< std::shared_ptr< AbstractSceneNode > > Children() { return m_children; }
    std::shared_ptr< AbstractSceneNode > Parent() { return m_parent; }

    virtual std::string Name() = 0;
    virtual Type GetType() = 0;
    virtual std::string Stringify() = 0;
protected:
    std::vector<std::shared_ptr<AbstractSceneNode>> m_children;
    std::shared_ptr< AbstractSceneNode > m_parent;
};

class RootNode : public AbstractSceneNode
{
public:
    std::string Name() override { return "Root"; }
    Type GetType() override { return Type::Actor; };
    std::string Stringify() override { return "Root"; }
};

class ActorNode : public AbstractSceneNode
{
public:
    ActorNode( std::shared_ptr< core::Actor > actor) : m_actor(actor)
    {
    }

    ~ActorNode() = default;

    std::string Name() { return std::string(m_actor->Name()); }
    Type GetType() { return Type::Actor;}
    std::string Stringify() { return std::string{}; /* TODO */ }
    std::shared_ptr<core::Actor> Actor() { return m_actor; }

    virtual void Init()
    {
        if(m_actor)
        {
           m_actor->Init();
           m_actor->PostInit();
        }
    }

    virtual void Shutdown()
    {
        if(m_actor)
            m_actor->Destroy();
    }

    virtual void OnUpdate()
    {
        if(m_actor)
            m_actor->Update();
    }
private:
    std::shared_ptr< core::Actor > m_actor;
};

class Scene
{
public:
    Scene() = default;
    ~Scene() = default;

    void Init();
    void Update();
    void Shutdown();

    void AddNode(std::shared_ptr<AbstractSceneNode> node);

    std::shared_ptr<AbstractSceneNode> NodeByName(std::string name);
    std::vector<std::shared_ptr<core::Actor> >  Actors();
    const RootNode& Root() { return m_root; }
private:
    std::map<std::string, std::shared_ptr<AbstractSceneNode>> m_nodes;
    RootNode m_root;
};
} // namespace hs::graphics
