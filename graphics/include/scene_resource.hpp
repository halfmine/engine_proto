#pragma once
#include "scene.hpp"
#include "resource_manager.hpp"
#include "filesystem.hpp"
#include "serialization.hpp"

#include <string_view>

namespace hs::graphics
{
class SceneResource : public resources::IResource
{
public:
    SceneResource(std::string scene_path);
    ~SceneResource() override;
    void Load() override;

    const char* RawResource(size_t& out_resource_size) override
    {
        out_resource_size = 0; // ?
        return reinterpret_cast<const char*>(m_scene.get());
    }

    std::string_view Name() override
    {
        return "Scene";
    }

    std::string_view FileName() override
    {
        return m_scene_path;
    }

    void Release() override
    {
        m_scene.reset();
        m_loaded = false;
    }

    std::shared_ptr< Scene > GetScene()
    {
        return m_scene;
    }

    bool IsLoaded() override
    {
        return m_loaded;
    }

private:
    core::SceneSerializer m_serializer;
    std::shared_ptr< Scene > m_scene;
    std::string m_scene_path;
    bool m_loaded;
};
}
