#pragma once
#include "scene.hpp"
#include "actor.hpp"
#include "serialization.hpp"
#include "shader.hpp"

namespace hs::graphics
{
class LightNode : public AbstractSceneNode
{
public:
    LightNode() = default;
    ~LightNode() override = default;

    void OnUpdate() override;
    std::string Name() override { return "light"; }
    Type GetType() override { return Type::Light; }
    std::string Stringify() override;
};

class ShaderNode : public AbstractSceneNode
{
public:
    enum class ShaderType
    {
        Vertex,
        Fragment
    };

    ShaderNode(std::string name, std::string resource_name, ShaderType type, std::shared_ptr<AbstractSceneNode> parent = nullptr)
        : AbstractSceneNode(parent), m_name(std::move(name)), m_resource_name(std::move(resource_name)), m_type(type)
    {
    }

    std::string Name() override
    {
        return m_name;
    }

    virtual Type GetType() override
    {
        return Type::Shader;
    }

    virtual std::string Stringify() override
    {
        return std::string{};
    }

    std::shared_ptr<ShaderResource> const& GetShader()
    {
        auto resource = std::dynamic_pointer_cast<ShaderResource>(resources::ResourceManager::Instance().ResourceByName(m_resource_name));
        if(!resource || !resource->IsLoaded())
            throw resources::ResourceException("Failed to find shader resource");
        return resource;
    }

    ShaderType GetShaderType()
    {
        return m_type;
    }

public:
    ShaderType m_type;
    std::string m_name;
    std::string m_resource_name;
};
} // hs::graphics
