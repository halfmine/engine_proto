#pragma once
#include "resource_manager.hpp"
#include "filesystem.hpp"
#include "component.hpp"
#include <memory>

namespace hs::graphics
{
class ShaderResource : public resources::IResource
{
public:
    ShaderResource(std::string filename, std::string name) : m_filename(std::move(filename)), m_name(std::move(name)), m_loaded(false)
    {
    }
    virtual ~ShaderResource() override = default;

    void Load() override;
    const char* RawResource(size_t& out_resource_size) override;
    std::string_view Name() override;
    std::string_view FileName() override { return m_filename; }
    void Release() override;
    bool IsLoaded() override { return m_loaded; }
    std::vector<char> const& ShaderContents() { return m_file.GetContents(); }
private:
    filesystem::File m_file;
    std::string m_name;
    std::string m_filename;
    bool m_loaded;
};
}
