#pragma once
#include "primitives.hpp"
#include "component.hpp"
#include "resource_manager.hpp"

namespace hs::graphics
{
struct Mesh
{
    /// mesh vertices
    std::vector<Vertex> vertices;

    /// mesh indices
    std::vector<int> indices;

    /// position and orientation
    math::Mat4x4 m_transform;
};

class MeshResource : public resources::IResource
{
public:
    MeshResource(std::string filename, std::string name) : m_filename(std::move(filename)), m_name(std::move(name)),
        m_loaded(false)
    {
    }

    MeshResource(MeshResource const& rhs) = default;
    MeshResource(MeshResource&& rhs) = default;
    MeshResource& operator=(MeshResource const& rhs) = default;
    MeshResource& operator=(MeshResource&& rhs) = default;

    void Load() override;
    const char *RawResource(size_t &out_resource_size) override;
    std::string_view Name() override;
    std::string_view FileName() override { return m_filename; }
    void Release() override { m_loaded = false; }
    bool IsLoaded() override { return m_loaded; }

    Mesh const& GetMesh() { return m_mesh; }
    Mesh* GetMeshPtr() { return &m_mesh; }
    void SetTransform(math::Mat4x4 const& transform) { m_mesh.m_transform = transform; }
private:
    std::string m_filename;
    std::string m_name;
    Mesh m_mesh;
    bool m_loaded;
};

/**
 * @brief The MeshComponent class. Loads from .obj file
 */
class MeshComponent : public core::Component
{
public:
    MeshComponent(std::string name, std::string resource_name) : core::Component(std::move(name), std::move(resource_name))
    {
    }

    Mesh GetMesh() const;
};
}
