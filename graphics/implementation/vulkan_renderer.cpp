#include <vulkan_renderer.hpp>
#include <app_exception.hpp>
#include <filesystem.hpp>
#include <primitives.hpp>
#include <mesh.hpp>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vector>
#include <string_view>
#include <string>
#include <exception>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <set>
#include <algorithm>
#include <vulkan/vulkan.hpp>
#include "resource_manager.hpp"
#include "scene.hpp"
#include "scene_resource.hpp"
#include "scene_nodes.hpp"
#include "shader.hpp"
#include "vulkan/vulkan_utils.hpp"
#include "vulkan/vulkan_instance.hpp"
#include "vulkan/vulkan_pipeline_builder.hpp"
#include "vulkan/vulkan_image_builder.hpp"
#include "vulkan/vulkan_core.hpp"
#include "vulkan/vulkan_swapchain.hpp"
#include "vulkan/vulkan_mesh_loader.hpp"
#include "vulkan/vulkan_commands.hpp"
#include "vk_mem_alloc.h"


using namespace std::literals;

namespace hs::graphics::vulkan
{
class VulkanRenderer::Impl
{
public:
   Impl(GLFWwindow* window) :
       m_window(window)
   {
       m_pressed_keys.reserve(5);
   }
   
   ~Impl() = default;

   std::vector<const char*> InitGlfwExtensions()
   {
       uint32_t glfwExtensionCount = 0;
       const char** glfwExtensions;
       glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
       std::vector<const char*> enabled_extension_names;
       for( auto i = 0u; i < glfwExtensionCount; ++i)
           enabled_extension_names.push_back(glfwExtensions[i]);
       return enabled_extension_names;
   }

   std::pair<VkPipelineLayout, VkPipeline> MakeSinglePipeline(std::string const& vertex_shader_resource_name, std::string const& fragment_shader_resource_name,
                                                              std::string const& material_name, std::vector<VkDescriptorSetLayout> descriptor_set_layouts)
   {
       VkShaderModule base_vertex_shader_module {VK_NULL_HANDLE};
       VkShaderModule base_fragment_shader_module {VK_NULL_HANDLE};
       auto& r_manager = resources::ResourceManager::Instance();

       auto vert_shader_resource = std::dynamic_pointer_cast<ShaderResource>(r_manager.ResourceByName(vertex_shader_resource_name));
       auto frag_shader_resource = std::dynamic_pointer_cast<ShaderResource>(r_manager.ResourceByName(fragment_shader_resource_name));
       if(!vert_shader_resource || !frag_shader_resource || !vert_shader_resource->IsLoaded() || !frag_shader_resource->IsLoaded())
           throw VulkanException("Failed to load one of shaders");

       base_vertex_shader_module = CreateShaderModule(vert_shader_resource->ShaderContents());
       base_fragment_shader_module = CreateShaderModule(frag_shader_resource->ShaderContents());
       std::vector< VkPipelineShaderStageCreateInfo > shader_stages {
           CreateShaderState(base_vertex_shader_module, VK_SHADER_STAGE_VERTEX_BIT),
           CreateShaderState(base_fragment_shader_module, VK_SHADER_STAGE_FRAGMENT_BIT)
       };


       VkPipelineLayout target_layout;
       VkPipeline target_pipeline;
       VulkanPipelineBuilder builder;
       builder.m_shader_stages = shader_stages;
       builder.m_input_assembly_state = CreateInputAssembly(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
       builder.m_viewport = CreateViewport(m_swapchain.m_info.m_extent);
       builder.m_scissor.offset = { 0, 0 };
       builder.m_scissor.extent = m_swapchain.m_info.m_extent;
       builder.m_rasterization_state = CreateRasterizationState(VK_POLYGON_MODE_FILL);
       builder.m_multisampling = CreateMultisampler(m_core.m_msaa_samples);
       builder.m_color_blend_attachment = CreateColorBlendAttachment();
       builder.m_layout = CreateLayout(m_core.m_device, descriptor_set_layouts);
       builder.m_depth_stencil = CreateDepthStencil(true, true, VK_COMPARE_OP_LESS_OR_EQUAL);

       target_pipeline = builder.Build(m_core.m_device, m_swapchain.m_render_pass);
       target_layout = builder.m_layout;

       CreateMaterial(target_pipeline, target_layout, material_name);

       vkDestroyShaderModule(m_core.m_device, base_fragment_shader_module, nullptr);
       vkDestroyShaderModule(m_core.m_device, base_vertex_shader_module, nullptr);
       return { target_layout, target_pipeline };
   }

   void CreatePipelines()
   {
       VkShaderModule base_vertex_shader_module {VK_NULL_HANDLE};
       VkShaderModule base_fragment_shader_module {VK_NULL_HANDLE};
       // TODO: приделать сцену, к которой можно прилеплять компоненты. Одним из компонентов будут шейдеры
       filesystem::File vertex_shader_file("shaders/triangle.vert.spv");
       filesystem::File fragment_shader_file("shaders/triangle.frag.spv");

       base_vertex_shader_module = CreateShaderModule(vertex_shader_file.GetContents());
       base_fragment_shader_module = CreateShaderModule(fragment_shader_file.GetContents());
       std::vector< VkPipelineShaderStageCreateInfo > shader_stages {
           CreateShaderState(base_vertex_shader_module, VK_SHADER_STAGE_VERTEX_BIT),
           CreateShaderState(base_fragment_shader_module, VK_SHADER_STAGE_FRAGMENT_BIT)
       };

       VulkanPipelineBuilder builder;
       builder.m_shader_stages = shader_stages;
       builder.m_input_assembly_state = CreateInputAssembly(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
       builder.m_viewport = CreateViewport(m_swapchain.m_info.m_extent);
       builder.m_scissor.offset = { 0, 0 };
       builder.m_scissor.extent = m_swapchain.m_info.m_extent;
       builder.m_rasterization_state = CreateRasterizationState(VK_POLYGON_MODE_FILL);
       builder.m_multisampling = CreateMultisampler(m_core.m_msaa_samples);
       builder.m_color_blend_attachment = CreateColorBlendAttachment();
       builder.m_layout = CreateLayout(m_core.m_device, m_commands.DescriptorSetLayout());
       builder.m_depth_stencil = CreateDepthStencil(true, true, VK_COMPARE_OP_LESS_OR_EQUAL);

       m_pipeline = builder.Build(m_core.m_device, m_swapchain.m_render_pass);
       m_pipeline_layout = builder.m_layout;

       CreateMaterial(m_pipeline, m_pipeline_layout, "main");

       vkDestroyShaderModule(m_core.m_device, base_fragment_shader_module, nullptr);
       vkDestroyShaderModule(m_core.m_device, base_vertex_shader_module, nullptr);


       MakeSinglePipeline("textured_shader_v", "textured_shader_f",
                           "default_texture", m_commands.TexturedSetLayout());
   }

   void RecreateSwapchain()
   {
       int width = 0;
       int height = 0;
       while (width == 0 && height == 0)
       {
           glfwGetFramebufferSize(m_window, &width, &height);
           glfwWaitEvents();
       }
       vkDeviceWaitIdle(m_core.m_device);

       CleanupSwapchain();
       m_swapchain.Build(m_core, m_allocator);
       CreatePipelines();
       // TODO: recreate commands
   }

   void CleanupSwapchain()
   {
        vkDestroyPipelineLayout(m_core.m_device, m_pipeline_layout, nullptr);
        vkDestroyPipeline(m_core.m_device, m_pipeline, nullptr);

        m_swapchain.Shutdown();
   }

   VkShaderModule CreateShaderModule(const std::vector<char>& shader_code)
   {
       VkShaderModuleCreateInfo create_info = {};
       create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
       create_info.codeSize = shader_code.size();
       create_info.pCode = reinterpret_cast<const uint32_t*>(shader_code.data());
       VkShaderModule shader_module;
       if( vkCreateShaderModule(m_core.m_device, &create_info, nullptr, &shader_module) != VK_SUCCESS)
           throw VulkanException("Cannot create shader module");
       return shader_module;
   }

   void LoadScene()
   {
       auto scene_ptr = std::dynamic_pointer_cast<SceneResource>(resources::ResourceManager::Instance().ResourceByName("Scene"));
       m_scene = scene_ptr->GetScene();
       if(!m_scene)
           throw VulkanException("Failed to pass scene to renderer");
       std::vector<std::shared_ptr<core::Actor>> actors = m_scene->Actors();
       for(auto& actor_ptr : actors)
       {
           auto component_w_ptr = actor_ptr->GetComponent("Mesh");
           if(component_w_ptr.expired())
               return;
           auto mesh_component_ptr = std::dynamic_pointer_cast<MeshComponent>(component_w_ptr.lock());
           if(!mesh_component_ptr)
           {
               DebugMessage("Failed to load mesh");
               return;
           }

           std::string resource_name(mesh_component_ptr->ResourceName());
           std::shared_ptr<VulkanMesh> mesh = std::make_shared<VulkanMesh>(resource_name);
           m_meshes[std::string(actor_ptr->Name())] = mesh;

           VulkanRenderable renderable;
           MeshLoader loader;
           loader.Load(m_core.m_device, m_core.m_graphics_queue, m_allocator, m_deletors, *mesh, m_commands);

//           auto material_component_w_ptr = actor_ptr->GetComponent("Material");
//           if(material_component_w_ptr.expired())
//           {
//               DebugMessage("Failed to find material for mesh");
//               return;
//           }
           // TODO: find material name and load


           renderable.m_mesh = mesh.get();
           renderable.m_material = GetMaterial("default_texture");
           renderable.m_render_matrix = actor_ptr->Identity();
           m_renderables.push_back(renderable);
       }

       VkSamplerCreateInfo sampler_info = CreateSamplerInfo(VK_FILTER_NEAREST);
       VkSampler block_sampler;
       vkCreateSampler(m_core.m_device, &sampler_info, nullptr, &block_sampler);

       // TODO: material system
       VulkanTexture tex("default_texture");
       tex.Load(m_core, m_commands, m_allocator, m_deletors);
//       m_textures["default_resource"] = tex;

       VkDescriptorSetAllocateInfo alloc_info = {};
       alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
       alloc_info.pNext = nullptr;
       alloc_info.descriptorPool = m_commands.DescriptorPool();
       alloc_info.descriptorSetCount = 1;
       alloc_info.pSetLayouts = m_commands.TextureLayout();

       auto* material = GetMaterial("default_texture");
       vkAllocateDescriptorSets(m_core.m_device, &alloc_info, &material->m_texture_set);

       VkDescriptorImageInfo desc_image_info;
       desc_image_info.sampler = block_sampler;
       desc_image_info.imageView = tex.m_image_view;
       desc_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
       VkWriteDescriptorSet texture_write = WriteDescriptorImage(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, material->m_texture_set, &desc_image_info, 0);
       vkUpdateDescriptorSets(m_core.m_device, 1, &texture_write, 0, nullptr);
   }

   void FillPhysicalDeviceProperties()
   {
       vkGetPhysicalDeviceProperties(m_core.m_physical_device, &m_device_properties);
       std::stringstream ss;
       ss << "Minimum buffer alignment is: " << m_device_properties.limits.minUniformBufferOffsetAlignment << std::endl;
       DebugMessage(ss.str());
   }

   VulkanMaterial* CreateMaterial(VkPipeline pipeline, VkPipelineLayout layout, std::string const& name)
   {
       VulkanMaterial material;
       material.m_pipeline = pipeline;
       material.m_pipeline_layout = layout;
       m_materials[name] = material;
       return &m_materials[name];
   }

   VulkanMaterial* GetMaterial(std::string const& name)
   {
       auto it = m_materials.find(name);
       if(it == m_materials.end())
           return nullptr;
       return &(it->second);
   }

   VulkanMesh* GetMesh(std::string const& name)
   {
       auto it = m_meshes.find(name);
       if(it == m_meshes.end())
           return nullptr;
       return it->second.get();
   }

   void Init()
   {
      m_core.m_app_name = "HS Engine prototype";
      m_core.m_engine_name = "Hot Sausages";
      m_core.m_validation_names = { "VK_LAYER_LUNARG_api_dump",
                                    "VK_LAYER_KHRONOS_validation",
                                    "VK_LAYER_LUNARG_device_simulation" };
      m_core.m_device_extensions = { "VK_KHR_swapchain", "VK_KHR_portability_subset" };
      m_core.m_enabled_extensions = InitGlfwExtensions();
      // TODO: enable  this extension only if it is present
      m_core.m_enabled_extensions.push_back("VK_KHR_get_physical_device_properties2");
      m_core.m_window = m_window;
      m_core.Build();
      FillPhysicalDeviceProperties();


      m_allocator = CreateAllocator(m_core.m_instance.GetInstance(), m_core.m_device, m_core.m_physical_device, m_deletors);

      m_swapchain.m_texture_mip_levels = 1;
      m_swapchain.Build(m_core, m_allocator);
      m_commands.Init(m_core, m_deletors);
      m_commands.InitDescriptorSets(m_core, m_device_properties, m_allocator, m_deletors);
      CreatePipelines();
      LoadScene();
   }

   void Run()
   {
       DebugMessage("Run called");
       if(m_scene)
           m_scene->Update();
       m_commands.Draw(m_allocator, m_core, m_swapchain, m_renderables, m_pressed_keys, m_device_properties);
   }

   void Shutdown()
   {
       vkDeviceWaitIdle(m_core.m_device);
       CleanupSwapchain();
       m_deletors.Flush();
       m_core.Shutdown();
   }

   void OnKeyPressed(core::Input::Key key)
   {
       m_pressed_keys.push_back(key);
   }

   void OnKeyReleased(core::Input::Key key)
   {
       auto it = std::find_if(m_pressed_keys.begin(), m_pressed_keys.end(),
                              [=](core::Input::Key key_ ){ return key == key_;});
       if(it != m_pressed_keys.end())
           m_pressed_keys.erase(it);
   }

private:
   VulkanCore m_core;
   VulkanSwapchain m_swapchain;

   VkPipeline m_pipeline;
   VkPipelineLayout m_pipeline_layout;

//   std::unordered_map<std::string, VulkanTexture> m_textures;

   VulkanCommands m_commands;
   std::shared_ptr<VulkanMesh> m_mesh_ptr = nullptr;
   std::shared_ptr<Scene> m_scene = nullptr;
   VkPhysicalDeviceProperties m_device_properties;

   std::vector<VulkanRenderable> m_renderables;
   std::map<std::string, std::shared_ptr<VulkanMesh>> m_meshes;
   std::map<std::string, VulkanMaterial> m_materials;

   GLFWwindow* m_window;

   DeletionStack m_deletors;
   VmaAllocator m_allocator;
   std::vector<core::Input::Key> m_pressed_keys;
};

void VulkanRenderer::Init()
{
   m_impl->Init();
}

void VulkanRenderer::Run()
{
   m_impl->Run();
}

void VulkanRenderer::Shutdown()
{
    m_impl->Shutdown();
}

const events::EventHandler<core::Input::Key>& VulkanRenderer::InputHandler()
{
    return m_input_handler;
}

const events::EventHandler<core::Input::Key>& VulkanRenderer::InputReleasedHandler()
{
    return m_input_release_handler;
}

VulkanRenderer::VulkanRenderer(void* window) : m_impl(std::make_unique<VulkanRenderer::Impl>(reinterpret_cast<GLFWwindow*>(window))),
                                              m_input_handler([this](core::Input::Key key)
                                              {
                                                  m_impl->OnKeyPressed(key);
                                              }),
                                              m_input_release_handler([this](core::Input::Key key)
                                              {
                                                  m_impl->OnKeyReleased(key);
                                              })
{
}

VulkanRenderer::~VulkanRenderer() = default;
} // namespace hs::graphics::vulkan
