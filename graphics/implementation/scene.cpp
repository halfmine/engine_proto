#include "scene.hpp"
#include "scene_nodes.hpp"

namespace hs::graphics
{

void Scene::Init()
{
    for(auto& node : m_nodes)
        if(node.second)
            node.second->Init();
}

void Scene::Update()
{
    for(auto& node : m_nodes)
        if(node.second)
            node.second->OnUpdate();
}

void Scene::Shutdown()
{
    for(auto& node : m_nodes)
        if(node.second)
            node.second->Shutdown();
}

void Scene::AddNode(std::shared_ptr<AbstractSceneNode> node)
{
    if(!node)
        return;
    if(node->Parent() == nullptr)
        m_root.AddChild(node);
    else
    {
        auto parent_ptr = node->Parent();
        auto it = m_nodes.find(parent_ptr->Name());
        if(it == m_nodes.end())
            return;
        it->second->AddChild(node);
    }
    m_nodes[node->Name()] = node;
}

std::shared_ptr<AbstractSceneNode> Scene::NodeByName(std::string name)
{
    auto it = m_nodes.find(name);
    if(it == m_nodes.end())
        return nullptr;
    return it->second;
}

std::vector<std::shared_ptr<core::Actor> > Scene::Actors()
{
    std::vector<std::shared_ptr<core::Actor>> actors;
    for(auto node_pair : m_nodes)
    {
        auto child_ptr = node_pair.second;
        if(!child_ptr)
            continue;
        if(child_ptr->GetType() == AbstractSceneNode::Type::Actor)
            actors.push_back(std::dynamic_pointer_cast<ActorNode>(child_ptr)->Actor());
    }
    return actors;
}

}
