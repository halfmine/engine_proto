#include "texture.hpp"
#include <stb/stb_image.h>

namespace hs::graphics
{

class TextureResource::Impl
{
public:
    Impl(std::string filename, std::string name) : m_filename(std::move(filename)), m_name(std::move(name)),
                                 m_pixels(nullptr), m_width(0), m_height(0), m_channels(0), m_loaded(false)
    {
    }

    void Load()
    {
        m_pixels = stbi_load(m_filename.data(), &m_width, &m_height, &m_channels, STBI_rgb_alpha);
        if(!m_pixels)
           throw resources::ResourceException("Cannot load texture");
        m_loaded = true;
    }

    const char* RawResource(size_t& out_resource_size)
    {
        out_resource_size = m_width * m_height * sizeof(stbi_uc);
        return reinterpret_cast<char const*>(m_pixels);
    }

    std::string_view Name()
    {
        return m_name;
    }

    std::string_view FileName()
    {
        return m_filename;
    }

    void Release()
    {
        if(m_pixels)
            stbi_image_free(reinterpret_cast<void*>(m_pixels));
        m_loaded = false;
    }

    bool IsLoaded()
    {
        return m_loaded;
    }

    const unsigned char* ImageInfo(int& width, int& height, int& channels)
    {
        width = m_width;
        height = m_height;
        channels = m_channels;
        return m_pixels;
    }
private:
    std::string m_filename;
    std::string m_name;
    stbi_uc* m_pixels;
    int m_width;
    int m_height;
    int m_channels;
    bool m_loaded;
};

TextureResource::TextureResource(std::string filename, std::string name) : p_impl(std::make_unique<Impl>(std::move(filename), std::move(name)))
{
}

TextureResource::~TextureResource() = default;

void TextureResource::Load()
{
    p_impl->Load();
}

const char* TextureResource::RawResource(size_t& out_resource_size)
{
    return p_impl->RawResource(out_resource_size);
}

std::string_view TextureResource::Name()
{
    return p_impl->Name();
}

std::string_view TextureResource::FileName()
{
    return p_impl->FileName();
}

void TextureResource::Release()
{
    p_impl->Release();
}

bool TextureResource::IsLoaded()
{
    return p_impl->IsLoaded();
}

const unsigned char* TextureResource::ImageInfo(int& width, int& height, int& channels)
{
    return p_impl->ImageInfo(width, height, channels);
}

}
