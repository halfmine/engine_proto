#include "shader.hpp"

namespace hs::graphics
{
void ShaderResource::Load()
{
    m_file.Load(m_filename);
    m_loaded = true;
}

const char *ShaderResource::RawResource(size_t& out_resource_size)
{
    out_resource_size = m_file.GetContents().size();
    return m_file.GetRawContents();
}

std::string_view ShaderResource::Name()
{
    return m_name;
}

void ShaderResource::Release()
{
    m_file.Close();
    m_loaded = false;
}

}
