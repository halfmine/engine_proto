#include <scene_resource.hpp>
#include <string_view>
#include <algorithm>
#include "serialization.hpp"
#include "mesh.hpp"
#include "scene_nodes.hpp"

namespace hs::graphics
{
SceneResource::SceneResource(std::string scene_path) : m_scene(nullptr), m_scene_path(std::move(scene_path)), m_loaded(false)
{
}

void SceneResource::Load()
{
    m_scene = m_serializer.Deserialize(m_scene_path);
    m_loaded = true;
}

SceneResource::~SceneResource() = default;
}
