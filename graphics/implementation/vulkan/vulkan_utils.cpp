#include  "vulkan_utils.hpp"

namespace hs::graphics::vulkan
{

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger)
{
    PFN_vkCreateDebugUtilsMessengerEXT func = reinterpret_cast< PFN_vkCreateDebugUtilsMessengerEXT >(vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"));
    if(func == nullptr)
       return VK_ERROR_EXTENSION_NOT_PRESENT;

    return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator)
{
    PFN_vkDestroyDebugUtilsMessengerEXT func = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT"));
    if(func == nullptr)
       return;
    func(instance, debugMessenger, pAllocator);
}

void CreateMessengerInfo(VkDebugUtilsMessengerCreateInfoEXT& messenger_info)
{
    messenger_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    messenger_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    messenger_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    messenger_info.pfnUserCallback = &debugCallback;
    messenger_info.pUserData = nullptr;
}

VkResult CreateImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags aspect_mask, VkImageView& image_view, const uint32_t, DeletionStack& deletors)
{
    VkImageViewCreateInfo image_view_info = {};
    image_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_info.image = image;
    image_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_info.format = format;
    image_view_info.subresourceRange.aspectMask = aspect_mask;
    image_view_info.subresourceRange.baseMipLevel = 0;
    image_view_info.subresourceRange.levelCount = 1;
    image_view_info.subresourceRange.baseArrayLayer = 0;
    image_view_info.subresourceRange.layerCount = 1;
    auto result = vkCreateImageView(device, &image_view_info, nullptr, &image_view);
    deletors.Push([=]()
    {
        vkDestroyImageView(device, image_view, nullptr);
    }, "DestroyImageView");

    return result;
}

VkFormat FindSupportedFormat(VkPhysicalDevice physical_device, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for(auto& format : candidates)
    {
       VkFormatProperties props;
       vkGetPhysicalDeviceFormatProperties(physical_device, format, &props);
       if(tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features))
          return format;
       else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features))
          return format;
    }

    throw VulkanException("Failed to find supported device format");
}

VkFormat FindDepthFormat(VkPhysicalDevice physical_device)
{
    return FindSupportedFormat(physical_device, {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
                               VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

uint32_t FindMemoryRequirements(VkPhysicalDevice physical_device, uint32_t type_filter, VkMemoryPropertyFlags flags)
{
    VkPhysicalDeviceMemoryProperties device_mem_props = {};
    vkGetPhysicalDeviceMemoryProperties(physical_device, &device_mem_props);
    for(uint32_t i = 0u; i < device_mem_props.memoryTypeCount; i++)
    {
       if(type_filter & (1 << i) &&
             device_mem_props.memoryTypes[i].propertyFlags & flags)
          return i;
    }

    throw VulkanException("Cannot find suitable memory type!");
}

VmaAllocator CreateAllocator(VkInstance instance, VkDevice device, VkPhysicalDevice physical_device, DeletionStack& deletors)
{
    VmaAllocator allocator;
    VmaAllocatorCreateInfo create_info = {};
    create_info.device = device;
    create_info.instance = instance;
    create_info.physicalDevice = physical_device;
    vmaCreateAllocator(&create_info, &allocator);
    deletors.Push([=]()
    {
        vmaDestroyAllocator(allocator);
    }, "Destroy VmaAllocator");
    return allocator;
}

QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface)
{
    QueueFamilyIndices indices;

    uint32_t queue_family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);
    std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());
    for(size_t i = 0; i < queue_families.size(); i++)
    {
       if(queue_families[i].queueCount > 0 && queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
       {
          indices.graphics_family = static_cast<uint32_t>(i);
       }

       VkBool32 present_support = false;
       vkGetPhysicalDeviceSurfaceSupportKHR(device, static_cast<uint32_t>(i), surface, &present_support);
       if(queue_families[i].queueCount > 0 && present_support)
          indices.present_family = i;

       if(indices.is_complete())
          break;
    }

    return indices;
}

SwapChainSupport QuerySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface)
{
    SwapChainSupport swapchain_support = {};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &swapchain_support.surface_capabilities);

    uint32_t format_count = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, nullptr);
    if(format_count != 0)
    {
       swapchain_support.formats.resize(format_count);
       vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, swapchain_support.formats.data());
    }

    uint32_t present_modes_count = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device , surface, &present_modes_count, nullptr);
    if(present_modes_count != 0)
    {
       swapchain_support.present_modes.resize(present_modes_count);
       vkGetPhysicalDeviceSurfacePresentModesKHR(device , surface, &present_modes_count, swapchain_support.present_modes.data());
    }

    return swapchain_support;
}

AllocatedBuffer CreateBuffer(VmaAllocator alloc, size_t size, VkBufferUsageFlags usage,
                             VmaMemoryUsage vma_usage, DeletionStack& deletors)
{
    VkBufferCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    info.pNext = nullptr;

    info.size = size;
    info.usage = usage;

    VmaAllocationCreateInfo alloc_info = {};
    alloc_info.usage = vma_usage;

    AllocatedBuffer buf;
    if(vmaCreateBuffer(alloc, &info, &alloc_info, &buf.m_buffer, &buf.m_allocation, nullptr) != VK_SUCCESS)
        throw VulkanException("Failed to allocate buffer");
    deletors.Push([=]()
    {
        vmaDestroyBuffer(alloc, buf.m_buffer, buf.m_allocation);
    }, "Vma destroy buffer");
    return buf;
}

VkDescriptorSetLayoutBinding CreateDescriptorSetLayoutBinding(VkDescriptorType type, VkShaderStageFlags stage_flags,
                                                              size_t binding_num)
{
    VkDescriptorSetLayoutBinding cam_buffer_binding = {};
    cam_buffer_binding.binding = binding_num;
    cam_buffer_binding.descriptorCount = 1;
    cam_buffer_binding.descriptorType = type;
    cam_buffer_binding.stageFlags = stage_flags;
    return cam_buffer_binding;
}

VkWriteDescriptorSet CreateWriteDescriptorSet(VkDescriptorType type, VkDescriptorSet dst_set,
                                              VkDescriptorBufferInfo* buffer_info, size_t binding_num)
{
    VkWriteDescriptorSet set_write = {};
    set_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    set_write.pNext = nullptr;

    set_write.dstBinding = binding_num;
    set_write.dstSet = dst_set;
    set_write.descriptorCount = 1;
    set_write.descriptorType = type;
    set_write.pBufferInfo = buffer_info;
    return set_write;
}

VkFenceCreateInfo MakeFenceInfo()
{
    VkFenceCreateInfo fence_info = {};
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.pNext = nullptr;
    return fence_info;
}

size_t PadUniformBufferSize(VkPhysicalDeviceProperties props, size_t original_size)
{
    size_t minimal_alignment = props.limits.minUniformBufferOffsetAlignment;
    size_t aligned = original_size;
    if(minimal_alignment > 0)
        aligned = (aligned + minimal_alignment - 1) & ~(minimal_alignment -1);
    return aligned;
}

VkSamplerCreateInfo CreateSamplerInfo(VkFilter filters, VkSamplerAddressMode addr_mode)
{
    VkSamplerCreateInfo sampler_info = {};
    sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_info.pNext = nullptr;
    sampler_info.magFilter = filters;
    sampler_info.minFilter = filters;
    sampler_info.addressModeU = addr_mode;
    sampler_info.addressModeV = addr_mode;
    sampler_info.addressModeW = addr_mode;
    return sampler_info;
}

VkWriteDescriptorSet WriteDescriptorImage(VkDescriptorType type, VkDescriptorSet dst_set, VkDescriptorImageInfo* image_info, uint32_t binding)
{
    VkWriteDescriptorSet write = {};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.pNext = nullptr;

    write.dstBinding = binding;
    write.dstSet = dst_set;
    write.descriptorCount = 1;
    write.descriptorType = type;
    write.pImageInfo = image_info;

    return write;
}

} // namespace hs::graphics::vulkan
