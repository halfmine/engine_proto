#pragma once
#include <string>
#include <vector>
#include <vulkan/vulkan.hpp>

namespace hs::graphics::vulkan
{
class VulkanInstance
{
public:
    void Init(std::string const& app_name, std::string const& engine_name,
              std::vector<const char*> validation_names, std::vector<const char*> enabled_extensions);
    void Shutdown() const;
    VkInstance GetInstance() { return m_instance; }
private:
    VkInstance m_instance;
};
} // namespace hs::graphics::vulkan
