#pragma once
#include <vulkan/vulkan.hpp>
#include <string>
#include <string_view>
#include <iostream>
#include <app_exception.hpp>
#include <primitives.hpp>
#include <texture.hpp>
#include <stack>
#include <functional>
#include <vk_mem_alloc.h>
#include "mesh.hpp"
#include "material.hpp"

namespace hs::graphics::vulkan
{
using namespace std::literals;

static void DebugMessage(std::string_view msg)
{
   std::cout << msg << std::endl;
}

static void DebugMessage(const char* msg)
{
   DebugMessage(std::string_view{ msg });
}

class VulkanException : public app_layer::AppException
{
public:
   VulkanException(const std::string msg) : app_layer::AppException(std::move(msg))
   {
   }

   VulkanException(const std::string_view msg) : app_layer::AppException(msg.data())
   {
   }

   VulkanException(const char* msg_chars) : app_layer::AppException(msg_chars)
   {
   }
};

class DeletionStack
{
public:
    using Task = std::function<void()>;

    template<typename Callable>
    void Push(Callable&& t, std::string description)
    {
        m_queue.push(std::make_pair(description, t));
    }

    void Flush()
    {
        while(!m_queue.empty())
        {
            auto t = m_queue.top();
            std::stringstream ss;
            ss << "DeletionStack: popping " << t.first << "." << std::endl;
            DebugMessage(ss.str());
            t.second();
            m_queue.pop();
        }
    }
private:
    std::stack<std::pair<std::string, Task>> m_queue;
};

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT /*messageType*/,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* /*pUserData*/)
{
   std::string_view severity {" "sv};
   switch(messageSeverity)
   {
   case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
      severity = "[INFO] "sv;
      break;
   case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
      severity = "[ERROR] "sv;
      break;
   case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
      severity = "[WARINING] "sv;
      break;
   case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
      severity = "[VERBOSE] "sv;
      break;
   default:
      severity = " "sv;
   }
   std::stringstream ss;
   ss << "[Validation layer]" << severity << pCallbackData->pMessage << std::endl;
   DebugMessage(ss.str());
   return VK_FALSE;
}

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

void CreateMessengerInfo(VkDebugUtilsMessengerCreateInfoEXT& messenger_info);

VkResult CreateImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags aspect_mask,
                                VkImageView& image_view, const uint32_t /*mip_levels*/, DeletionStack& deletors);

VkFormat FindSupportedFormat(VkPhysicalDevice physical_device, const std::vector<VkFormat>& candidates,
                                    VkImageTiling tiling, VkFormatFeatureFlags features);

VkFormat FindDepthFormat(VkPhysicalDevice physical_device);

uint32_t FindMemoryRequirements(VkPhysicalDevice physical_device, uint32_t type_filter, VkMemoryPropertyFlags flags);

VmaAllocator CreateAllocator(VkInstance instance, VkDevice device,
                             VkPhysicalDevice physical_device, DeletionStack& deletors);

struct QueueFamilyIndices
{
   std::optional<uint32_t> graphics_family;
   std::optional<uint32_t> present_family;

   bool is_complete() { return graphics_family.has_value() && present_family.has_value(); }
};

QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);

struct SwapChainSupport
{
   std::vector<VkSurfaceFormatKHR> formats;
   std::vector<VkPresentModeKHR> present_modes;
   VkSurfaceCapabilitiesKHR surface_capabilities;
   char unused[4];
};

SwapChainSupport QuerySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);

struct AllocatedBuffer
{
    VmaAllocation m_allocation;
    VkBuffer m_buffer;
};

AllocatedBuffer CreateBuffer(VmaAllocator alloc, size_t size, VkBufferUsageFlags usage,
                             VmaMemoryUsage vma_usage, DeletionStack& deletors);

VkDescriptorSetLayoutBinding CreateDescriptorSetLayoutBinding(VkDescriptorType type,
                                                              VkShaderStageFlags stage_flags, size_t binding_num);

VkWriteDescriptorSet CreateWriteDescriptorSet(VkDescriptorType type, VkDescriptorSet dst_set,
                                                     VkDescriptorBufferInfo* buffer_info, size_t binding_num);

VkFenceCreateInfo MakeFenceInfo();

struct AllocatedImage
{
    VkImage m_image;
    VmaAllocation m_allocation;
};

struct VulkanMesh
{
    VulkanMesh(std::string resource_name) : m_resource_name(std::move(resource_name))
    {
    }

    Mesh GetMesh()
    {
        auto resource_ptr = std::dynamic_pointer_cast<MeshResource>(resources::ResourceManager::Instance().ResourceByName(m_resource_name));
        if(!resource_ptr || !resource_ptr->IsLoaded())
            throw VulkanException("Failed to find mesh resource");
        return resource_ptr->GetMesh();
    }

    std::string m_resource_name;
    AllocatedBuffer m_allocated_buffer;
};

struct VulkanMaterial
{
    std::string m_resource_name;
    VkPipeline m_pipeline;
    VkPipelineLayout m_pipeline_layout;
    VkDescriptorSet m_texture_set { VK_NULL_HANDLE };
};

struct VulkanRenderable
{
    VulkanMaterial*  m_material = nullptr;
    VulkanMesh* m_mesh = nullptr;
    glm::mat4 m_render_matrix;
};

struct FrameData
{
    VkSemaphore m_present_semaphore;
    VkSemaphore m_render_semaphore;
    VkFence m_render_fence;

    VkCommandPool m_command_pool;
    VkCommandBuffer m_command_buffer;

    AllocatedBuffer m_camera_buffer;
    VkDescriptorSet m_desciptor_set;

    AllocatedBuffer m_objects_buffer;
    VkDescriptorSet m_objects_descriptor_set;
};

struct GpuObject
{
    glm::mat4 m_model;
};

struct VertexDescription
{
    std::vector<VkVertexInputBindingDescription> m_binding_descrs;
    std::vector<VkVertexInputAttributeDescription> m_attr_descr = {};

    void Create()
    {
        VkVertexInputBindingDescription main_descr = {};
        main_descr.binding = 0;
        main_descr.stride = sizeof(Vertex);
        main_descr.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        m_binding_descrs.push_back(main_descr);

        VkVertexInputAttributeDescription pos_descr = {};
        pos_descr.binding = 0;
        pos_descr.location = 0;
        pos_descr.format = VK_FORMAT_R32G32B32_SFLOAT;
        pos_descr.offset = offsetof(graphics::Vertex, position);
        m_attr_descr.push_back(pos_descr);

        VkVertexInputAttributeDescription normal_descr;
        normal_descr.binding = 0;
        normal_descr.location = 1;
        normal_descr.format = VK_FORMAT_R32G32_SFLOAT;
        normal_descr.offset = offsetof(graphics::Vertex, normal);
        m_attr_descr.push_back(normal_descr);

        VkVertexInputAttributeDescription color_descr = {};
        color_descr.binding = 0;
        color_descr.location = 2;
        color_descr.format = VK_FORMAT_R32G32B32_SFLOAT;
        color_descr.offset = offsetof(graphics::Vertex, color);
        m_attr_descr.push_back(color_descr);

        VkVertexInputAttributeDescription uv_description = {};
        uv_description.binding = 0;
        uv_description.location = 3;
        uv_description.format = VK_FORMAT_R32G32_SFLOAT;
        uv_description.offset = offsetof(graphics::Vertex, uv);
        m_attr_descr.push_back(uv_description);
    }
};

struct SceneBuffer
{
    glm::vec4 m_fog_color;
    glm::vec4 m_fog_distances;
    glm::vec4 m_ambient_color;
    glm::vec4 m_sunlight_direction;
    glm::vec4 m_sunlight_color;
};


size_t PadUniformBufferSize(VkPhysicalDeviceProperties props, size_t original_size);

VkSamplerCreateInfo CreateSamplerInfo(VkFilter filters,
                                      VkSamplerAddressMode addr_mode =VK_SAMPLER_ADDRESS_MODE_REPEAT);
VkWriteDescriptorSet WriteDescriptorImage(VkDescriptorType type, VkDescriptorSet dst_set, VkDescriptorImageInfo* image_info, uint32_t binding);

} // namespace hs::graphics::vulkan
