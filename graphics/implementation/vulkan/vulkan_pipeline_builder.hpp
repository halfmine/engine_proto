#pragma once
#include <vulkan/vulkan.hpp>
#include "vulkan_utils.hpp"
#include "primitives.hpp"
#include <array>

namespace hs::graphics::vulkan
{
VkPipelineShaderStageCreateInfo CreateShaderState(VkShaderModule module, VkShaderStageFlagBits flags);
VkPipelineVertexInputStateCreateInfo CreateVertexInputState();
VkPipelineInputAssemblyStateCreateInfo CreateInputAssembly(VkPrimitiveTopology topology);
VkViewport CreateViewport(VkExtent2D extent);
VkPipelineRasterizationStateCreateInfo CreateRasterizationState(VkPolygonMode polygone_mode);
VkPipelineMultisampleStateCreateInfo CreateMultisampler(VkSampleCountFlagBits msaa_samples);
VkPipelineColorBlendAttachmentState CreateColorBlendAttachment();
VkPipelineLayout CreateLayout(VkDevice device, std::vector<VkDescriptorSetLayout> descriptor_set_layouts);
VkPipelineDepthStencilStateCreateInfo CreateDepthStencil(bool depth_test, bool depth_write, VkCompareOp compare_op);

struct VertexInputState
{
    VertexDescription m_descr;
    VkPipelineVertexInputStateCreateInfo vertex_state_info = {};
    VkPipelineVertexInputStateCreateInfo m_info;

    void CreateInfo();
};


class VulkanPipelineBuilder
{
public:
    std::vector<VkPipelineShaderStageCreateInfo> m_shader_stages;
    VertexInputState m_vertex_input_state;
    VkPipelineInputAssemblyStateCreateInfo m_input_assembly_state;
    VkViewport m_viewport;
    VkRect2D m_scissor;
    VkPipelineRasterizationStateCreateInfo m_rasterization_state;
    VkPipelineColorBlendAttachmentState m_color_blend_attachment;
    VkPipelineMultisampleStateCreateInfo m_multisampling;
    VkPipelineLayout m_layout;
    VkPipelineDepthStencilStateCreateInfo m_depth_stencil;

    VkPipeline Build(VkDevice device, VkRenderPass pass);
};
} // namespace hs::graphics::vulkan
