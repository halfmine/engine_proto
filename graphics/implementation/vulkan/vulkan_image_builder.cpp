#include "vulkan_image_builder.hpp"
#include "vulkan_commands.hpp"

namespace hs::graphics::vulkan
{

void VulkanTexture::Load(VulkanCore& core, VulkanCommands& commands, VmaAllocator alloc, DeletionStack& deletors)
{
    auto resource_ptr = std::dynamic_pointer_cast<TextureResource>(resources::ResourceManager::Instance().ResourceByName(m_resource_name));
    if(!resource_ptr || !resource_ptr->IsLoaded())
        throw VulkanException("Failed to load texture resource");

    VkFormat format = VK_FORMAT_R8G8B8A8_SRGB;
    int width = 0;
    int height = 0;
    int channels = 0;
    size_t image_size = 0;
    resource_ptr->ImageInfo(width, height, channels);
    char const* image_data = resource_ptr->RawResource(image_size);
    image_size = width * height * 4;
    AllocatedBuffer staging_buffer = CreateBuffer(alloc, image_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY, deletors);

    void* data = nullptr;
    vmaMapMemory(alloc, staging_buffer.m_allocation, &data);
    memcpy(data, image_data, image_size);
    vmaUnmapMemory(alloc, staging_buffer.m_allocation);

    resources::ResourceManager::Instance().ReleaseResource(m_resource_name);
    VkExtent3D extent;
    extent.width = width;
    extent.height = height;
    extent.depth = 1;

    m_image_builder.format = format;
    m_image_builder.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    m_image_builder.vma_usage = VMA_MEMORY_USAGE_GPU_ONLY;
    m_image_builder.m_extent = extent;
    m_image_builder.mem_properties = 0;

    AllocatedImage gpu_image;
    m_image_builder.Build(deletors, gpu_image, alloc);

    commands.ImmediateSubmit(core.m_device, core.m_graphics_queue, [=](VkCommandBuffer cmd)
    {
        VkImageSubresourceRange range;
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.levelCount = 1;

        VkImageMemoryBarrier transfer_barrier = {};
        transfer_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        transfer_barrier.pNext = nullptr;

        transfer_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        transfer_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        transfer_barrier.image = gpu_image.m_image;
        transfer_barrier.subresourceRange = range;

        transfer_barrier.srcAccessMask = 0;
        transfer_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             0, 0, nullptr, 0, nullptr, 1, &transfer_barrier);

        VkBufferImageCopy copy_region = {};
        copy_region.bufferOffset = 0;
        copy_region.bufferImageHeight = 0;
        copy_region.bufferRowLength = 0;

        copy_region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        copy_region.imageSubresource.baseArrayLayer = 0;
        copy_region.imageSubresource.mipLevel = 0;
        copy_region.imageSubresource.layerCount = 1;
        copy_region.imageExtent = extent;

        vkCmdCopyBufferToImage(cmd, staging_buffer.m_buffer, gpu_image.m_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_region);

        VkImageMemoryBarrier copy_barrier = transfer_barrier;
        copy_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        copy_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        copy_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        copy_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                             0, 0, nullptr, 0, nullptr, 1, &copy_barrier);

    });

    m_image = gpu_image;
    CreateImageView(core.m_device, m_image.m_image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, m_image_view, 0, deletors);
}

} // namespace hs::graphics::vulkan
