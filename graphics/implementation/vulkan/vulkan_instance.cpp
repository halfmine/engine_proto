#include "vulkan_instance.hpp"
#include "vulkan_utils.hpp"

namespace hs::graphics::vulkan
{
void VulkanInstance::Init(const std::string& app_name, const std::string& engine_name,
                                  std::vector<const char*> validation_names, std::vector<const char*> enabled_extensions)
{
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = app_name.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = engine_name.c_str();
    appInfo.engineVersion = VK_MAKE_VERSION(1, 5, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;
    VkInstanceCreateInfo create_info = {};

#ifdef ENABLE_VALIDATION_LAYERS
  VkDebugUtilsMessengerCreateInfoEXT debug_messenger_for_instance_info = {};
  create_info.enabledLayerCount = static_cast<uint32_t>(validation_names.size());
  create_info.ppEnabledLayerNames = validation_names.data();
  enabled_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
  CreateMessengerInfo(debug_messenger_for_instance_info);
  create_info.pNext = reinterpret_cast<void*>(&debug_messenger_for_instance_info);
#else
  create_info.enabledLayerCount = 0;
  create_info.pNext = nullptr;
#endif
   create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
   create_info.pApplicationInfo = &appInfo;
   create_info.enabledExtensionCount = static_cast<uint32_t>(enabled_extensions.size());
   create_info.ppEnabledExtensionNames = enabled_extensions.data();
   VkResult result = vkCreateInstance(&create_info, nullptr, &m_instance);
   if(result != VK_SUCCESS)
   {
       std::stringstream err_text;
       err_text << "Failed to create Vulkan instance with error code " << result;
       throw VulkanException(err_text.str());
   }
}

void VulkanInstance::Shutdown() const
{
    vkDestroyInstance(m_instance, nullptr);
}
}
