#include "vulkan_commands.hpp"
#include <cstdlib>
#include <glm/gtx/transform.hpp>
#include "input.hpp"
#include "camera.hpp"

namespace hs::graphics::vulkan
{
static uint64_t TIMEOUT = 1000000000;
static size_t MAX_OBJECTS_COUNT = 10000;

void VulkanCommands::Init(VulkanCore const& core, DeletionStack& deletors)
{
    for(auto i = 0u; i < FRAME_COUNT; ++i)
    {
        CreateCommandPools(core.m_device, core.m_physical_device, core.m_surface, i, deletors);
        CreateCommandBuffers(core.m_device, i);
        InitSync(core.m_device, deletors, i);
    }
    VkFenceCreateInfo upload_context_fence_info = MakeFenceInfo();
    if(vkCreateFence(core.m_device, &upload_context_fence_info, nullptr, &m_upload_context.m_fence) != VK_SUCCESS)
        throw VulkanException("Failed to create upload context fence");
    deletors.Push([=]()
    {
        vkDestroyFence(core.m_device, m_upload_context.m_fence, nullptr);
    }, "Destroying upload context fence");
}

void VulkanCommands::InitDescriptorSets(VulkanCore const& core, VkPhysicalDeviceProperties props,
                                        VmaAllocator alloc, DeletionStack& deletors)
{
    const auto scene_buffer_size = FRAME_COUNT * PadUniformBufferSize(props, sizeof(SceneBuffer));
    m_scene_buffer = CreateBuffer(alloc, scene_buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, deletors);

    std::vector<VkDescriptorPoolSize> sizes = {
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 10},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 10},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 10},
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 10}
    };
    VkDescriptorPoolCreateInfo descriptor_pool_info = {};
    descriptor_pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_info.pNext = nullptr;

    descriptor_pool_info.pPoolSizes = sizes.data();
    descriptor_pool_info.poolSizeCount = sizes.size();
    descriptor_pool_info.flags = 0;
    descriptor_pool_info.maxSets = 10;

    vkCreateDescriptorPool(core.m_device, &descriptor_pool_info, nullptr, &m_descriptor_pool);
    deletors.Push([=]()
    {
        vkDestroyDescriptorPool(core.m_device, m_descriptor_pool, nullptr);
    }, "Destroy descriptor pool");

    VkDescriptorSetLayoutBinding objects_binding = CreateDescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT, 0);
    VkDescriptorSetLayoutCreateInfo objects_layout_info = {};
    objects_layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    objects_layout_info.pNext = nullptr;
    objects_layout_info.bindingCount = 1;
    objects_layout_info.pBindings = &objects_binding;
    objects_layout_info.flags = 0;
    vkCreateDescriptorSetLayout(core.m_device, &objects_layout_info, nullptr, &m_objects_layout);
    deletors.Push([=]()
    {
        vkDestroyDescriptorSetLayout(core.m_device, m_objects_layout, nullptr);
    }, "Destroying descriptor set layout");


    VkDescriptorSetLayoutBinding cam_buffer_binding = CreateDescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT, 0);
    VkDescriptorSetLayoutBinding scene_info_binding = CreateDescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
                                                                                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 1);
    std::array<VkDescriptorSetLayoutBinding, 2> bindings = { cam_buffer_binding, scene_info_binding };

    VkDescriptorSetLayoutCreateInfo layout_info = {};
    layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layout_info.pNext = nullptr;
    layout_info.bindingCount = bindings.size();
    layout_info.flags = 0;
    layout_info.pBindings = bindings.data();
    vkCreateDescriptorSetLayout(core.m_device, &layout_info, nullptr, &m_descriptors_layout);
    deletors.Push([=]()
    {
        vkDestroyDescriptorSetLayout(core.m_device, m_descriptors_layout, nullptr);
    }, "Destroying descriptor set layout");

    VkDescriptorSetLayoutBinding texture_binding = CreateDescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT, 0);
    VkDescriptorSetLayoutCreateInfo texture_layout_create_info = {};
    texture_layout_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    texture_layout_create_info.pNext = nullptr;
    texture_layout_create_info.bindingCount =1;
    texture_layout_create_info.pBindings = &texture_binding;
    texture_layout_create_info.flags = 0;
    vkCreateDescriptorSetLayout(core.m_device, &texture_layout_create_info, nullptr, &m_texture_layout);
    deletors.Push([=]()
    {
        vkDestroyDescriptorSetLayout(core.m_device, m_texture_layout, nullptr);
    }, "Destroy texture descriptor layout");

    for(auto i = 0u; i < FRAME_COUNT; ++i)
    {
        m_frames[i].m_camera_buffer = CreateBuffer(alloc, sizeof(Camera),
                                                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, deletors);
        m_frames[i].m_objects_buffer = CreateBuffer(alloc, sizeof(GpuObject) * MAX_OBJECTS_COUNT, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, deletors);

        VkDescriptorSetAllocateInfo objects_alloc_info = {};
        objects_alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        objects_alloc_info.pNext = nullptr;
        objects_alloc_info.descriptorPool = m_descriptor_pool;
        objects_alloc_info.pSetLayouts = &m_objects_layout;
        objects_alloc_info.descriptorSetCount = 1;
        vkAllocateDescriptorSets(core.m_device, &objects_alloc_info, &m_frames[i].m_objects_descriptor_set);

        VkDescriptorSetAllocateInfo alloc_info = {};
        alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        alloc_info.pNext = nullptr;
        alloc_info.descriptorPool = m_descriptor_pool;
        alloc_info.descriptorSetCount = 1;
        alloc_info.pSetLayouts = &m_descriptors_layout;
        vkAllocateDescriptorSets(core.m_device, &alloc_info, &m_frames[i].m_desciptor_set);

        VkDescriptorBufferInfo objects_buffer_info = {};
        objects_buffer_info.buffer = m_frames[i].m_objects_buffer.m_buffer;
        objects_buffer_info.offset = 0;
        objects_buffer_info.range = sizeof(GpuObject) * MAX_OBJECTS_COUNT;

        VkDescriptorBufferInfo cam_buffer_info = {};
        cam_buffer_info.buffer = m_frames[i].m_camera_buffer.m_buffer;
        cam_buffer_info.offset = 0;
        cam_buffer_info.range = sizeof(Camera);

        VkDescriptorBufferInfo scene_buffer_info = {};
        scene_buffer_info.buffer = m_scene_buffer.m_buffer;
        scene_buffer_info.offset = 0;
        scene_buffer_info.range = sizeof(SceneBuffer);

        VkWriteDescriptorSet objects_set_write = CreateWriteDescriptorSet(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, m_frames[i].m_objects_descriptor_set, &objects_buffer_info, 0);
        VkWriteDescriptorSet cam_set_write = CreateWriteDescriptorSet(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, m_frames[i].m_desciptor_set, &cam_buffer_info, 0);
        VkWriteDescriptorSet scene_set_write = CreateWriteDescriptorSet(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, m_frames[i].m_desciptor_set, &scene_buffer_info, 1);
        std::array<VkWriteDescriptorSet, 3> set_writes = {objects_set_write, cam_set_write, scene_set_write};
        vkUpdateDescriptorSets(core.m_device, set_writes.size(), set_writes.data(), 0, nullptr);
    }
}

void VulkanCommands::Draw(VmaAllocator alloc, VulkanCore const& core, VulkanSwapchain const& swapchain,
                          std::vector<VulkanRenderable> const&  renderables, std::vector<core::Input::Key> const& pressed_keys, VkPhysicalDeviceProperties props)
{
    VkResult res = VK_SUCCESS;
    auto& current_frame = CurrentFrame();
    res = vkWaitForFences(core.m_device, 1, &current_frame.m_render_fence, true, TIMEOUT);
    if( res != VK_SUCCESS ) throw VulkanException("Failed to draw: wait for render fence");
    res = vkResetFences(core.m_device, 1, &current_frame.m_render_fence);
    if( res != VK_SUCCESS ) throw VulkanException("Failed to draw: reset render fence");

    uint32_t swapchain_image_index = 0;
    res = vkAcquireNextImageKHR(core.m_device, swapchain.m_info.m_swapchain, TIMEOUT, current_frame.m_present_semaphore,
                             nullptr, &swapchain_image_index);
    if( res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR ) throw VulkanException("Failed to draw: acquire next image");
    res = vkResetCommandBuffer(current_frame.m_command_buffer, 0);
    if( res != VK_SUCCESS ) throw VulkanException("Failed to draw: reset command buffer");

    BeginCommandBuffer();

    VkClearValue clear_value;
    auto flash = abs(sin(m_frame_number / 120.f));
    clear_value.color = { {0.0f, 0.0f, flash, 1.0f} };

    VkClearValue depth_clear;
    depth_clear.depthStencil.depth = 1.0f;

    std::array<VkClearValue, 2> clear_values = { clear_value, depth_clear };

    VkRenderPassBeginInfo render_pass_begin_info = {};
    render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_begin_info.pNext = nullptr;
    render_pass_begin_info.renderPass = swapchain.m_render_pass;
    render_pass_begin_info.renderArea.offset.x = 0;
    render_pass_begin_info.renderArea.offset.y = 0;
    render_pass_begin_info.renderArea.extent = swapchain.m_info.m_extent;
    render_pass_begin_info.framebuffer = swapchain.m_swapchain_frame_buffers[swapchain_image_index];
    render_pass_begin_info.clearValueCount = clear_values.size();
    render_pass_begin_info.pClearValues = clear_values.data();

    // Commands begin
    vkCmdBeginRenderPass(current_frame.m_command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

    DrawRenderables(renderables, pressed_keys, alloc, props);

    vkCmdEndRenderPass(current_frame.m_command_buffer);

    // Commands end
    res = vkEndCommandBuffer(current_frame.m_command_buffer);
    SubmitCommandBuffer(core.m_graphics_queue);
    DisplayFrame(swapchain, core.m_graphics_queue, swapchain_image_index);

    if(res != VK_SUCCESS)
        throw VulkanException("Failed to draw frame");
    m_frame_number++;
}

void VulkanCommands::DrawRenderables(std::vector<VulkanRenderable> const& renderables, std::vector<core::Input::Key> const& pressed_keys,
                                     VmaAllocator alloc, VkPhysicalDeviceProperties props)
{
    HandleCameraInput(pressed_keys);
    glm::mat4 view = glm::translate(glm::mat4(1.0f), m_camera_position);

    view = glm::rotate(view, glm::radians(m_pitch), glm::vec3(0, 1, 0));
    view = glm::rotate(view, glm::radians(m_yaw), glm::vec3(1, 0, 0));
    glm::mat4 projection = glm::perspective(glm::radians(70.0f), 1700.0f / 900.0f, 0.1f, 200.0f);
    projection[1][1] *= -1;

    Camera cam;
    cam.m_projection = projection;
    cam.m_view = view;
    cam.m_view_proj = projection * view;

    auto& current_frame = CurrentFrame();

    float framed = m_frame_number / 120.0f;
    m_scene_buffer_data.m_ambient_color = { sin(framed), 0, cos(framed), 1 };

    void* object_data = nullptr;
    vmaMapMemory(alloc, current_frame.m_objects_buffer.m_allocation, &object_data);
    GpuObject* obj = reinterpret_cast<GpuObject*>(object_data);

    for(auto i = 0u; i < renderables.size(); ++i)
    {
        obj[i].m_model = renderables[i].m_render_matrix;
    }
    vmaUnmapMemory(alloc, current_frame.m_objects_buffer.m_allocation);

    char* scene_data = nullptr;
    vmaMapMemory(alloc, m_scene_buffer.m_allocation, reinterpret_cast<void**>(&scene_data));
    int frame_index = m_frame_number % FRAME_COUNT;
    scene_data += PadUniformBufferSize(props, sizeof(SceneBuffer)) *frame_index;
    memcpy(scene_data, &m_scene_buffer_data, sizeof(SceneBuffer));
    vmaUnmapMemory(alloc, m_scene_buffer.m_allocation);

    void* cam_data = nullptr;
    vmaMapMemory(alloc, current_frame.m_camera_buffer.m_allocation, &cam_data);
    memcpy(cam_data, &cam, sizeof(Camera));
    vmaUnmapMemory(alloc, current_frame.m_camera_buffer.m_allocation);

    VulkanMesh* last_mesh = nullptr;
    VulkanMaterial* last_material = nullptr;
    int i = 0;
    for(auto& renderable : renderables)
    {
        if(renderable.m_material != last_material)
        {
            vkCmdBindPipeline(current_frame.m_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, renderable.m_material->m_pipeline);
            last_material = renderable.m_material;
            vkCmdBindDescriptorSets(current_frame.m_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, renderable.m_material->m_pipeline_layout,
                                    1, 1, &current_frame.m_objects_descriptor_set, 0, nullptr);

            uint32_t buffer_offset = static_cast<uint32_t>(PadUniformBufferSize(props, sizeof(SceneBuffer)) * frame_index);
            vkCmdBindDescriptorSets(current_frame.m_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, renderable.m_material->m_pipeline_layout,
                                    0, 1, &current_frame.m_desciptor_set, 1, &buffer_offset);
            if(renderable.m_material->m_texture_set != VK_NULL_HANDLE)
            {
                vkCmdBindDescriptorSets(current_frame.m_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, renderable.m_material->m_pipeline_layout,
                                        2, 1, &renderable.m_material->m_texture_set, 0, nullptr);
            }
        }


        if(!renderable.m_mesh)
            throw VulkanException("Failed: Passed empty mesh resource");

        if(renderable.m_mesh != last_mesh)
        {
            VkDeviceSize offset = 0;
            vkCmdBindVertexBuffers(current_frame.m_command_buffer, 0, 1, &renderable.m_mesh->m_allocated_buffer.m_buffer, &offset);
            last_mesh = renderable.m_mesh;
        }
        vkCmdDraw(current_frame.m_command_buffer, renderable.m_mesh->GetMesh().vertices.size(), 1, 0, i);
        ++i;
    }
}

void VulkanCommands::HandleCameraInput(const std::vector<core::Input::Key>& pressed_keys)
{
    for(auto& key : pressed_keys)
    {
        switch(key)
        {
        case core::Input::Key::LEFT:
            m_pitch += 1.0f;
            break;
        case core::Input::Key::A:
            m_camera_position.x += 0.3f;
            break;
        case core::Input::Key::RIGHT:
            m_pitch -= 1.0f;
            break;
        case core::Input::Key::D:
            m_camera_position.x -= 0.3f;
            break;
        case core::Input::Key::UP:
            m_yaw += 1.0f;
            break;
        case core::Input::Key::W:
            m_camera_position.z -= 0.3f;
            break;
        case core::Input::Key::DOWN:
            m_yaw -= 1.0f;
            break;
        case core::Input::Key::S:
            m_camera_position.z += 0.3f;
            break;
        case core::Input::Key::E:
            m_camera_position.y += 0.1f;
            break;
        case core::Input::Key::Q:
            m_camera_position.y -= 0.1f;
            break;
        default:
            break;
        }
    }
}


void VulkanCommands::ImmediateSubmit(VkDevice device, VkQueue graphics_queue, std::function<void (VkCommandBuffer)>&& func)
{
    VkCommandBufferAllocateInfo command_buffer_info = {};
    command_buffer_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_info.pNext = nullptr;
    command_buffer_info.commandPool = m_upload_context.m_pool;
    command_buffer_info.commandBufferCount = 1;
    VkCommandBuffer cmd_buf;
    if(vkAllocateCommandBuffers(device, &command_buffer_info, &cmd_buf) != VK_SUCCESS)
        throw VulkanException("Failed to create upload context command buffer");

    VkCommandBufferBeginInfo begin_info = {};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.pInheritanceInfo = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(cmd_buf, &begin_info);

    func(cmd_buf);

    vkEndCommandBuffer(cmd_buf);

    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buf;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    if(vkQueueSubmit(graphics_queue, 1, &submit_info, m_upload_context.m_fence) != VK_SUCCESS)
        throw VulkanException("Failed to submit commands");
    vkWaitForFences(device, 1, &m_upload_context.m_fence, VK_TRUE, TIMEOUT);
    vkResetFences(device, 1, &m_upload_context.m_fence);

    vkResetCommandPool(device, m_upload_context.m_pool, 0);
}

void VulkanCommands::CreateCommandPools(VkDevice device, VkPhysicalDevice physical_device, VkSurfaceKHR surface, size_t frame_index, DeletionStack& deletors)
{
    VkCommandPoolCreateInfo create_info = {};
    auto family_indices = FindQueueFamilies(physical_device, surface);
    create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    create_info.queueFamilyIndex = family_indices.graphics_family.value();
    create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    if(vkCreateCommandPool(device, &create_info, nullptr, &m_frames[frame_index].m_command_pool) != VK_SUCCESS)
        throw VulkanException("Cannot create command pool");

    VkCommandPoolCreateInfo upload_context_pool_create_info = {};
    upload_context_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    upload_context_pool_create_info.queueFamilyIndex = family_indices.graphics_family.value();
    upload_context_pool_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    if(vkCreateCommandPool(device, &upload_context_pool_create_info, nullptr, &m_upload_context.m_pool) != VK_SUCCESS)
        throw VulkanException("Failed to create upload context command pool");

    deletors.Push([=]()
    {
        vkDestroyCommandPool(device, m_upload_context.m_pool, nullptr);
        vkDestroyCommandPool(device, m_frames[frame_index].m_command_pool, nullptr);
    }, "Destroy command pool");
}

void VulkanCommands::CreateCommandBuffers(VkDevice device, size_t frame_index)
{
    VkCommandBufferAllocateInfo command_buffer_info = {};
    command_buffer_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_info.pNext = nullptr;
    command_buffer_info.commandPool = m_frames[frame_index].m_command_pool;
    command_buffer_info.commandBufferCount = 1;
//    command_buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    if(vkAllocateCommandBuffers(device, &command_buffer_info, &m_frames[frame_index].m_command_buffer) != VK_SUCCESS)
        throw VulkanException("Failed to create main command buffer");
}

void VulkanCommands::BeginCommandBuffer()
{
    VkCommandBufferBeginInfo begin_info = {};
    auto& current_frame = CurrentFrame();
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.pInheritanceInfo = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(current_frame.m_command_buffer, &begin_info);
}

void VulkanCommands::SubmitCommandBuffer(VkQueue graphics_queue)
{
    VkSubmitInfo submit_info = {};
    VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    auto& current_frame = CurrentFrame();

    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &current_frame.m_command_buffer;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &current_frame.m_render_semaphore;
    submit_info.pWaitDstStageMask = &wait_stage;
    if(vkQueueSubmit(graphics_queue, 1, &submit_info, current_frame.m_render_fence) != VK_SUCCESS)
        throw VulkanException("Failed to submit commands");
}

void VulkanCommands::DisplayFrame(VulkanSwapchain const& swapchain, VkQueue graphics_queue, uint32_t image_index)
{
    VkPresentInfoKHR present_info = {};
    auto& current_frame = CurrentFrame();
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = nullptr;
    present_info.pSwapchains = &swapchain.m_info.m_swapchain;
    present_info.swapchainCount = 1;
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &current_frame.m_render_semaphore;
    present_info.pImageIndices = &image_index;

    vkQueuePresentKHR(graphics_queue, &present_info);
}

FrameData& VulkanCommands::CurrentFrame()
{
    return m_frames[m_frame_number % FRAME_COUNT];
}

void VulkanCommands::InitSync(VkDevice device, DeletionStack& deletors, size_t frame_index)
{
    VkFenceCreateInfo fence_info = MakeFenceInfo();
    fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    if(vkCreateFence(device, &fence_info, nullptr, &m_frames[frame_index].m_render_fence) != VK_SUCCESS)
        throw VulkanException("Failed to create render fence");

    VkSemaphoreCreateInfo semaphore_info = {};
    semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphore_info.pNext = nullptr;
    semaphore_info.flags = 0;
    if(vkCreateSemaphore(device, &semaphore_info, nullptr, &m_frames[frame_index].m_render_semaphore) != VK_SUCCESS)
        throw VulkanException("Failed to create semaphore");
    if(vkCreateSemaphore(device, &semaphore_info, nullptr, &m_frames[frame_index].m_present_semaphore) != VK_SUCCESS)
        throw VulkanException("Failed to create semaphore");
    deletors.Push([=]()
    {
        vkDestroySemaphore(device, m_frames[frame_index].m_present_semaphore, nullptr);
        vkDestroySemaphore(device, m_frames[frame_index].m_render_semaphore, nullptr);
        vkDestroyFence(device, m_frames[frame_index].m_render_fence, nullptr);
    }, "Destroy semaphores and fence");
}
} // namespace hs::graphics::vulkan
