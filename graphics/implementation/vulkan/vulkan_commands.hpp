#pragma once
#include "vulkan_utils.hpp"
#include "vulkan_core.hpp"
#include "vulkan_swapchain.hpp"
#include "input.hpp"
#include <functional>

namespace hs::graphics::vulkan
{
constexpr size_t FRAME_COUNT = 2;

struct UploadContext
{
    VkFence m_fence;
    VkCommandPool m_pool;
};

class VulkanRenderer;

class VulkanCommands
{
public:

    void Init(VulkanCore const& core, DeletionStack& deletors);
    void InitDescriptorSets(VulkanCore const& core, VkPhysicalDeviceProperties props, VmaAllocator alloc, DeletionStack& deletors);
    void Draw(VmaAllocator alloc, VulkanCore const& core, VulkanSwapchain const& swapchain, std::vector<VulkanRenderable> const& renderables,
              std::vector<core::Input::Key> const& pressed_keys, VkPhysicalDeviceProperties props);
    void DrawRenderables(std::vector<VulkanRenderable> const& renderables, std::vector<core::Input::Key> const& pressed_keys, VmaAllocator alloc, VkPhysicalDeviceProperties props);
    void HandleCameraInput(std::vector<core::Input::Key> const& pressed_keys);
    void AllocateMaterialDescriptorSet(VulkanCore const& core, VmaAllocator alloc, DeletionStack& deletors, VulkanMaterial* materials);

    std::vector<VkDescriptorSetLayout> DescriptorSetLayout()
    {
        std::vector<VkDescriptorSetLayout> res;
        res.reserve(2);
        res.push_back(m_descriptors_layout);
        res.push_back(m_objects_layout);
        return res;
    }
    std::vector<VkDescriptorSetLayout> TexturedSetLayout()
    {
        auto vec = DescriptorSetLayout();
        vec.push_back(m_texture_layout);
        return vec;
    }

    void ImmediateSubmit(VkDevice device, VkQueue graphics_queue, std::function<void(VkCommandBuffer cmd)> && func);
    VkDescriptorPool DescriptorPool() { return m_descriptor_pool; }
    VkDescriptorSetLayout* TextureLayout() { return &m_texture_layout; }

private:
    void InitSync(VkDevice device, DeletionStack& deletors, size_t frame_index);
    void CreateCommandPools(VkDevice device, VkPhysicalDevice physical_device, VkSurfaceKHR surface, size_t frame_index, DeletionStack& deletors);
    void CreateCommandBuffers(VkDevice device, size_t frame_index);
    void BeginCommandBuffer();
    void SubmitCommandBuffer(VkQueue graphics_queue);
    void DisplayFrame(VulkanSwapchain const& swapchain, VkQueue graphics_queue, uint32_t image_index);

    FrameData& CurrentFrame();

    std::array<FrameData, FRAME_COUNT> m_frames;

    VkDescriptorSetLayout m_descriptors_layout;
    VkDescriptorSetLayout m_objects_layout;
    VkDescriptorSetLayout m_texture_layout;
    VkDescriptorPool m_descriptor_pool;

    AllocatedBuffer m_scene_buffer;
    SceneBuffer m_scene_buffer_data;

    UploadContext m_upload_context;

    int m_frame_number = 0;
    glm::vec3 m_camera_position = { 0.0f, 0.0f, -6.f };
    float m_yaw = 0.0f;
    float m_pitch = 0.0f;
};
} // namespace hs::graphics::vulkan
