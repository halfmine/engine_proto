#include "vulkan_pipeline_builder.hpp"
#include "vulkan_utils.hpp"

namespace hs::graphics::vulkan
{
VkPipelineShaderStageCreateInfo CreateShaderState(VkShaderModule module, VkShaderStageFlagBits flags)
{
    VkPipelineShaderStageCreateInfo vertex_stage_create_info = {};
    vertex_stage_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertex_stage_create_info.stage = flags;
    vertex_stage_create_info.module = module;
    vertex_stage_create_info.pName = "main";
    return vertex_stage_create_info;
}


void VertexInputState::CreateInfo()
{
    m_descr.Create();

    m_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    m_info.vertexBindingDescriptionCount = m_descr.m_binding_descrs.size();
    m_info.pVertexBindingDescriptions = m_descr.m_binding_descrs.data();
    m_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(m_descr.m_attr_descr.size());
    m_info.pVertexAttributeDescriptions = m_descr.m_attr_descr.data();
    m_info.flags = 0;
    m_info.pNext = nullptr;
}

VkPipelineInputAssemblyStateCreateInfo CreateInputAssembly(VkPrimitiveTopology topology)
{
    VkPipelineInputAssemblyStateCreateInfo input_assembly_state = {};
    input_assembly_state.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_assembly_state.topology = topology;
    input_assembly_state.primitiveRestartEnable = VK_FALSE;
    return input_assembly_state;
}

VkViewport CreateViewport(VkExtent2D extent)
{
    VkViewport viewport = {};
    viewport.x = 0;
    viewport.y = 0;
    viewport.width = extent.width;
    viewport.height = extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    return viewport;
}

VkPipelineRasterizationStateCreateInfo CreateRasterizationState(VkPolygonMode polygone_mode)
{
    VkPipelineRasterizationStateCreateInfo rasterizer_state_create_info = {};
    rasterizer_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer_state_create_info.pNext = nullptr;
    rasterizer_state_create_info.depthClampEnable = VK_FALSE;
    rasterizer_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterizer_state_create_info.polygonMode = polygone_mode;
    rasterizer_state_create_info.lineWidth = 1.0f;
    rasterizer_state_create_info.cullMode = VK_CULL_MODE_NONE;
    rasterizer_state_create_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer_state_create_info.depthBiasEnable = VK_FALSE;
    rasterizer_state_create_info.depthBiasClamp = 0.0f;
    rasterizer_state_create_info.depthBiasConstantFactor = 0.0f;
    rasterizer_state_create_info.depthBiasSlopeFactor = 0.0f;

    return rasterizer_state_create_info;
}

VkPipelineMultisampleStateCreateInfo CreateMultisampler(VkSampleCountFlagBits /*msaa_samples*/)
{
    VkPipelineMultisampleStateCreateInfo multisample_create_info = {};
    multisample_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisample_create_info.pNext = nullptr;

    multisample_create_info.sampleShadingEnable = VK_FALSE;
    multisample_create_info.minSampleShading = 1.0f;
    multisample_create_info.pSampleMask = nullptr;
    multisample_create_info.alphaToOneEnable = VK_FALSE;
    multisample_create_info.alphaToCoverageEnable = VK_FALSE;
    multisample_create_info.rasterizationSamples = /*msaa_samples*/VK_SAMPLE_COUNT_1_BIT;
    return multisample_create_info;
}

VkPipelineColorBlendAttachmentState CreateColorBlendAttachment()
{
    VkPipelineColorBlendAttachmentState color_blend_attachment = {};
    color_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_A_BIT | VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
    color_blend_attachment.blendEnable = VK_FALSE;
    return color_blend_attachment;
}

VkPipelineDepthStencilStateCreateInfo CreateDepthStencil(bool depth_test, bool depth_write, VkCompareOp compare_op)
{
    VkPipelineDepthStencilStateCreateInfo depth_stencil_create_info = {};
    depth_stencil_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depth_stencil_create_info.depthTestEnable = depth_test ? VK_TRUE : VK_FALSE;
    depth_stencil_create_info.depthWriteEnable = depth_write? VK_TRUE : VK_FALSE;
    depth_stencil_create_info.depthCompareOp = depth_test ? compare_op : VK_COMPARE_OP_ALWAYS;
    depth_stencil_create_info.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_create_info.minDepthBounds = 0.0f;
    depth_stencil_create_info.maxDepthBounds = 1.0f;
    depth_stencil_create_info.stencilTestEnable = VK_FALSE;
    return depth_stencil_create_info;
}

VkPipeline VulkanPipelineBuilder::Build(VkDevice device, VkRenderPass pass)
{
    m_vertex_input_state.CreateInfo();

    VkPipelineViewportStateCreateInfo viewport_state_create_info = {};
    viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state_create_info.pScissors = &m_scissor;
    viewport_state_create_info.scissorCount = 1;
    viewport_state_create_info.pViewports = &m_viewport;
    viewport_state_create_info.viewportCount = 1;

    VkPipelineColorBlendStateCreateInfo color_blend_create_info = {};
    color_blend_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blend_create_info.logicOpEnable = VK_FALSE;
    color_blend_create_info.attachmentCount = 1;
    color_blend_create_info.pAttachments = &m_color_blend_attachment;

    VkGraphicsPipelineCreateInfo pipeline_create_info = {};
    pipeline_create_info.flags = 0;
    pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_create_info.stageCount = m_shader_stages.size();
    pipeline_create_info.pStages = m_shader_stages.data();
    pipeline_create_info.pVertexInputState = &m_vertex_input_state.m_info;
    pipeline_create_info.pInputAssemblyState = &m_input_assembly_state;
    pipeline_create_info.pViewportState = &viewport_state_create_info;
    pipeline_create_info.pRasterizationState = &m_rasterization_state;
    pipeline_create_info.pMultisampleState = &m_multisampling;
    pipeline_create_info.pDepthStencilState = nullptr;
    pipeline_create_info.pColorBlendState = &color_blend_create_info;
    pipeline_create_info.pDynamicState = nullptr;
    pipeline_create_info.pTessellationState = nullptr;
    pipeline_create_info.pNext = nullptr;

    pipeline_create_info.layout = m_layout;
    pipeline_create_info.renderPass = pass;
    pipeline_create_info.subpass = 0u;
    pipeline_create_info.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_create_info.basePipelineIndex = 0;
    pipeline_create_info.pDepthStencilState = &m_depth_stencil;

    VkPipeline result_pipeline;
    if(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_create_info, nullptr, &result_pipeline) != VK_SUCCESS)
       throw VulkanException("Cannot create graphics pipeline!");
    return result_pipeline;
}

VkPipelineLayout CreateLayout(VkDevice device, std::vector<VkDescriptorSetLayout> descriptor_set_layouts)
{
       VkPipelineLayout layout;
       VkPipelineLayoutCreateInfo pipeline_layout_create_info = {};
       pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
       pipeline_layout_create_info.pPushConstantRanges = nullptr;
       pipeline_layout_create_info.pushConstantRangeCount = 0;
       pipeline_layout_create_info.setLayoutCount = descriptor_set_layouts.size();
       pipeline_layout_create_info.pSetLayouts = descriptor_set_layouts.data();
       if(vkCreatePipelineLayout(device, &pipeline_layout_create_info, nullptr, &layout) != VK_SUCCESS)
           throw VulkanException("Could not create pipeline layout");
       return layout;
}
} // namespace hs::graphics::vulkan
