#include "vulkan_core.hpp"
#include <strstream>
#include <algorithm>
#include <set>
#include <string_view>

namespace hs::graphics::vulkan
{
namespace
{
bool CheckValidationLayers(std::vector<const char*> const& validation_names)
{
    uint32_t layer_count;
    vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

    std::vector<VkLayerProperties> layers(layer_count);
    vkEnumerateInstanceLayerProperties(&layer_count, layers.data());
    std::stringstream layer_names;
    for(auto i = 0u; i < layer_count; i++)
    {
       layer_names << layers[i].layerName << std::endl;
    }
    DebugMessage(layer_names.str());

    for(const char* layer_name : validation_names)
    {
        auto it = std::find_if(layers.begin(), layers.end(), [&layer_name](const auto& layer)
        {
            return strcmp(layer.layerName, layer_name);
        });

        if(it == layers.end())
        {
            std::stringstream ss;
            ss << "Could not find " << layer_name << std::endl;
            throw VulkanException(ss.str());
        }
    }

    return true;
}

void SetupDebugMessenger(VkInstance instance, VkDebugUtilsMessengerEXT& debug_messenger)
{
#ifdef ENABLE_VALIDATION_LAYERS
    VkDebugUtilsMessengerCreateInfoEXT messenger_info = {};
    CreateMessengerInfo(messenger_info);

    VkResult messenger = CreateDebugUtilsMessengerEXT(instance, &messenger_info, nullptr, &debug_messenger);
    if( messenger != VK_SUCCESS)
       throw VulkanException("Failed to create debug messenger");
#endif
}

void InitInstance(VulkanInstance& instance, std::vector<char const*> const& validation_names,
                  std::vector<char const*> const& enabled_extensions)
{
#ifdef ENABLE_VALIDATION_LAYERS
    if(!CheckValidationLayers(validation_names))
        throw VulkanException("Requested validation layers were not found");
#endif
    std::string app_name = "HS Engine prototype";
    std::string engine_name = "Hot Sausages";
    instance.Init(app_name, engine_name, validation_names, enabled_extensions);
}

VkSurfaceKHR CreateSurface(VkInstance instance, GLFWwindow* window)
{
    VkSurfaceKHR surface;
    std::stringstream ss;

    auto result = glfwCreateWindowSurface(instance, window, nullptr, &surface);
    if(result != VK_SUCCESS)
       throw VulkanException("Failed to create window surface");
    return surface;
}

bool CheckDeviceExtensionsSupport(VkPhysicalDevice device, std::vector<char const*> const& device_extensions)
{
    uint32_t extensions_count = 0;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensions_count, nullptr);
    std::vector<VkExtensionProperties> device_supported_extensions(extensions_count);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensions_count, device_supported_extensions.data());
    std::stringstream supported_extensions;
    for(auto ext : device_supported_extensions)
    {
       supported_extensions << "Device supported extension: " << ext.extensionName << std::endl;
    }
    DebugMessage(supported_extensions.str());

    std::set<std::string_view> required_extensions(device_extensions.begin(), device_extensions.end());
    for(const auto& ext : device_supported_extensions)
       required_extensions.erase(ext.extensionName);

    if(!required_extensions.empty())
    {
       std::stringstream required_extensions_stream;
       for(auto ext : required_extensions)
       {
          required_extensions_stream << "Missing required device extension: " << ext << std::endl;
       }
       std::stringstream ss;
       ss << "Missing required device extensions!" << required_extensions_stream.str();
       throw VulkanException(ss.str());
    }
    return required_extensions.empty();
}

bool IsDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR surface, std::vector<char const*> const& device_extensions)
{
    VkPhysicalDeviceProperties device_props;
    vkGetPhysicalDeviceProperties(device, &device_props);

    VkPhysicalDeviceFeatures device_features;
    vkGetPhysicalDeviceFeatures(device, &device_features);

    bool suitable = (device_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
            || device_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
           && /*device_features.geometryShader &&*/ device_features.samplerAnisotropy;

    QueueFamilyIndices indices = FindQueueFamilies(device, surface);
    bool extensions_supported = CheckDeviceExtensionsSupport(device, device_extensions);

    bool device_swapchain_support = false;
    if(extensions_supported)
    {
       SwapChainSupport swapchain_support = QuerySwapChainSupport(device, surface);
       device_swapchain_support = !swapchain_support.formats.empty() && !swapchain_support.present_modes.empty();
    }
    return suitable &&
          indices.is_complete() &&
          extensions_supported &&
          device_swapchain_support;
}

VkSampleCountFlagBits GetMaxSampleCount(VkPhysicalDevice device)
{
   VkPhysicalDeviceProperties props;
   vkGetPhysicalDeviceProperties(device, &props);

   VkSampleCountFlags counts = props.limits.framebufferColorSampleCounts & props.limits.framebufferDepthSampleCounts;
       if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
       if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
       if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
       if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
       if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
       if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }

   return VK_SAMPLE_COUNT_1_BIT;
}

void PickPhysicalDevice(VkInstance instance, VkSurfaceKHR surface, std::vector<char const*> const& device_extensions,
                        VkPhysicalDevice& physical_device, VkSampleCountFlagBits& msaa_samples)
{
    uint32_t device_count = 0;
    vkEnumeratePhysicalDevices(instance, &device_count, nullptr);
    if(device_count == 0)
       throw VulkanException("Cannot find any physical devices.");
    std::vector<VkPhysicalDevice> available_devices;

    available_devices.resize(device_count);
    vkEnumeratePhysicalDevices(instance, &device_count, available_devices.data());
    for(auto& device : available_devices)
    {
       if(IsDeviceSuitable(device, surface, device_extensions))
       {
          physical_device = device;
          msaa_samples = GetMaxSampleCount(physical_device);
          break;
       }
    }

    if(physical_device == VK_NULL_HANDLE)
       throw VulkanException("Cannot find any suitable device");
}

VkDevice CreateLogicalDevice(VkPhysicalDevice physical_device, VkSurfaceKHR surface,
                             std::vector<char const*> const& device_extensions, std::vector<char const*> const& validation_names,
                             VkQueue& graphics_queue, VkQueue& present_queue)
{
    QueueFamilyIndices indices = FindQueueFamilies(physical_device, surface);

    std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
    std::set<uint32_t> unique_families = { indices.present_family.value(), indices.graphics_family.value() };
    float queue_priority = 1.0f;
    for(auto queue_family : unique_families)
    {
        // Queue info
       VkDeviceQueueCreateInfo queue_create_info = {};
       queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
       queue_create_info.queueFamilyIndex = queue_family;
       queue_create_info.queueCount = 1;
       queue_create_info.pQueuePriorities = &queue_priority;
       queue_create_infos.push_back(queue_create_info);
    }

    // Device features info
    VkPhysicalDeviceFeatures physical_device_features = {};
    physical_device_features.samplerAnisotropy = VK_TRUE;

    // Device create info
    VkDeviceCreateInfo device_create_info = {};
    device_create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
    device_create_info.pQueueCreateInfos = queue_create_infos.data();
    device_create_info.pEnabledFeatures = &physical_device_features;
    device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

    // Backwards compatibility with extensions
    device_create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
    device_create_info.ppEnabledExtensionNames = device_extensions.data();
#ifdef ENABLE_VALIDATION_LAYERS
    device_create_info.enabledLayerCount = static_cast<uint32_t>(validation_names.size());
    device_create_info.ppEnabledLayerNames = validation_names.data();
#else
    device_create_info.enabledLayerCount = 0;
#endif

    VkDevice device;
    auto res = vkCreateDevice(physical_device, &device_create_info, nullptr, &device);
    if(res!= VK_SUCCESS)
    {
       std::string err_ = "Could not create Vulkan logical device: " + std::to_string(res);
       throw VulkanException(err_.c_str());
    }

    vkGetDeviceQueue(device, indices.graphics_family.value(), 0, &graphics_queue);
    vkGetDeviceQueue(device, indices.present_family.value(), 0, &present_queue);
    return device;
}
} // anonymous namespace


void VulkanCore::Build()
{
    InitInstance(m_instance, m_validation_names, m_enabled_extensions);
    SetupDebugMessenger(m_instance.GetInstance(), m_debug_messenger);
    m_surface = CreateSurface(m_instance.GetInstance(), m_window);

    PickPhysicalDevice(m_instance.GetInstance(), m_surface,
                       m_device_extensions, m_physical_device, m_msaa_samples);

    m_device = CreateLogicalDevice(m_physical_device, m_surface, m_device_extensions,
                                   m_validation_names, m_graphics_queue, m_present_queue);
}

void VulkanCore::Shutdown()
{
    vkDestroyDevice(m_device, nullptr);
#ifdef ENABLE_VALIDATION_LAYERS
       DestroyDebugUtilsMessengerEXT(m_instance.GetInstance(), m_debug_messenger, nullptr);
#endif
    vkDestroySurfaceKHR(m_instance.GetInstance(), m_surface, nullptr);
    vkDestroyInstance(m_instance.GetInstance(), nullptr);
}

}
