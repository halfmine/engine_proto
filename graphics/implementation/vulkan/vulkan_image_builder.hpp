#pragma once
#include <vulkan/vulkan.hpp>
#include "vulkan_utils.hpp"
#include "vulkan_core.hpp"
#include "texture.hpp"

namespace hs::graphics::vulkan
{
struct VulkanImageBuilder
{
    VkExtent3D m_extent;
    uint32_t mip_levels = 1;
    VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT;
    VkFormat format;
    VkImageUsageFlags usage;
    VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL;
    VkMemoryPropertyFlags mem_properties;
    VmaMemoryUsage vma_usage;

    void Build(DeletionStack& deletion_stack,
               AllocatedImage& output_image, VmaAllocator& alloc)
    {
        VkImageCreateInfo image_create_info = {};
        image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        image_create_info.pNext = nullptr;
        image_create_info.imageType = VK_IMAGE_TYPE_2D;
        image_create_info.format = format;
        image_create_info.extent = m_extent;
        image_create_info.mipLevels = 1;
        image_create_info.arrayLayers = 1;
        image_create_info.tiling = tiling;
        image_create_info.usage = usage;
        image_create_info.samples = sample_count;

        VmaAllocationCreateInfo alloc_info = {};
        alloc_info.usage = vma_usage;
        alloc_info.requiredFlags = mem_properties;

        vmaCreateImage(alloc, &image_create_info, &alloc_info,
                       &output_image.m_image, &output_image.m_allocation, nullptr);
        deletion_stack.Push([=]()
        {
            vmaDestroyImage(alloc, output_image.m_image, output_image.m_allocation);
        }, "Destroy Image");
    }
};

struct Image
{
    AllocatedImage m_image;
    VkImageView m_image_view;
    VkFormat m_format;
    VkImageUsageFlags m_flags;
    VkImageAspectFlags m_image_view_aspect_flags;
    VmaMemoryUsage m_vma_usage;

    void Create(VulkanCore const& core, DeletionStack& deletors, VmaAllocator& alloc, VkExtent3D image_extent)
    {
        VulkanImageBuilder builder;
        builder.format = m_format;
        builder.vma_usage = m_vma_usage;
        builder.m_extent = image_extent;
        builder.usage = m_flags;
        builder.mem_properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        builder.Build(deletors, m_image, alloc);
        auto res = CreateImageView(core.m_device, m_image.m_image, m_format,
                                   m_image_view_aspect_flags, m_image_view, 1, deletors );
        if(res != VK_SUCCESS)
            throw VulkanException("Failed to create image");
    }
};

class VulkanCommands;

struct VulkanTexture
{
    VulkanTexture(std::string resource_name) : m_resource_name(std::move(resource_name))
    {
    }

    void Load(VulkanCore& core, VulkanCommands& commands, VmaAllocator alloc, DeletionStack& deletors);

    std::string m_resource_name;
    VulkanImageBuilder m_image_builder;
    AllocatedImage m_image;
    VkImageView m_image_view;
};
}
