#pragma once
#include "vulkan_instance.hpp"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include "vulkan_utils.hpp"

namespace hs::graphics::vulkan
{
class VulkanCore
{
public:
    VulkanCore() = default;
    ~VulkanCore() = default;

    void Build();
    void Shutdown();

    std::string m_app_name;
    std::string m_engine_name;
    std::vector<const char*> m_validation_names;
    std::vector<const char*> m_device_extensions;
    std::vector<const char*> m_enabled_extensions;
    GLFWwindow* m_window;
    VulkanInstance m_instance;
    VkDevice m_device;
    VkPhysicalDevice m_physical_device;
    VkSurfaceKHR m_surface;
    VkDebugUtilsMessengerEXT m_debug_messenger;
    VkSampleCountFlagBits m_msaa_samples;

    VkQueue m_graphics_queue;
    VkQueue m_present_queue;

    SwapChainSupport m_swapchain_support;
};


} // namespace hs::graphics::vulkan
