#include "vulkan_swapchain.hpp"
#include "vulkan_image_builder.hpp"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace hs::graphics::vulkan
{
namespace
{
VkSurfaceFormatKHR ChooseSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& available_formats)
{
    for(const auto& format : available_formats)
    {
       if(format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
          return format;
    }

    VulkanException("Cannot find specified surface format!");
    return available_formats.front();
}

VkPresentModeKHR ChoosePresentMode(const std::vector<VkPresentModeKHR>& available_modes)
{
    for(const auto& mode : available_modes)
    {
       if(mode == VK_PRESENT_MODE_MAILBOX_KHR)
          return mode;
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, GLFWwindow* window)
{
    if(capabilities.currentExtent.width == std::numeric_limits<uint32_t>::max())
       return capabilities.currentExtent;

    int width, height;
    glfwGetWindowSize(window, &width, &height);
    VkExtent2D actual_extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
    actual_extent.width = std::max(capabilities.minImageExtent.width,
                                   std::min(capabilities.maxImageExtent.width, actual_extent.width));
    actual_extent.height = std::max(capabilities.minImageExtent.height,
                                    std::min(capabilities.maxImageExtent.height, actual_extent.height));
    std::stringstream ss;
    ss << "Swap extent is " << actual_extent.width << "x" << actual_extent.height << "." << std::endl;
    DebugMessage(ss.str());
    return actual_extent;
}

SwapchainInfo CreateSwapchain(VkPhysicalDevice physical_device, VkDevice device, VkSurfaceKHR surface, GLFWwindow* window,
                              std::vector<VkImage>& swapchain_images, DeletionStack& deletors)
{
    SwapchainInfo info;
    SwapChainSupport swapchain_support = QuerySwapChainSupport(physical_device, surface);
    VkSurfaceFormatKHR surface_format = ChooseSurfaceFormat(swapchain_support.formats);
    VkPresentModeKHR present_mode = ChoosePresentMode(swapchain_support.present_modes);
    auto extent = ChooseSwapExtent(swapchain_support.surface_capabilities, window);

    uint32_t image_count = swapchain_support.surface_capabilities.minImageCount + 1;
    if(swapchain_support.surface_capabilities.maxImageCount > 0 && image_count > swapchain_support.surface_capabilities.maxImageCount)
       image_count = swapchain_support.surface_capabilities.maxImageCount;
    VkSwapchainCreateInfoKHR create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    create_info.surface = surface;
    create_info.minImageCount = image_count;
    create_info.imageFormat = surface_format.format;
    create_info.imageColorSpace = surface_format.colorSpace;
    create_info.imageArrayLayers = 1;
    create_info.imageExtent = extent;
    create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    QueueFamilyIndices indices = FindQueueFamilies(physical_device, surface);
    if(indices.graphics_family != indices.present_family)
    {
       create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
       create_info.queueFamilyIndexCount = 2;
       uint32_t queue_family_indices[] = {indices.present_family.value(), indices.graphics_family.value()};
       create_info.pQueueFamilyIndices = queue_family_indices;
    }
    else
       create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    create_info.preTransform = swapchain_support.surface_capabilities.currentTransform;
    create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    create_info.presentMode = present_mode;
    create_info.clipped = VK_TRUE;
    create_info.oldSwapchain = VK_NULL_HANDLE;

    if(vkCreateSwapchainKHR(device, &create_info, nullptr, &info.m_swapchain) != VK_SUCCESS)
       throw VulkanException("Could not create swap chain!");

    vkGetSwapchainImagesKHR(device, info.m_swapchain, &image_count, nullptr);
    swapchain_images.resize(image_count);
    vkGetSwapchainImagesKHR(device, info.m_swapchain, &image_count, swapchain_images.data());
    info.m_format = surface_format.format;
    info.m_extent = extent;

    deletors.Push([=]()
    {
        vkDestroySwapchainKHR(device, info.m_swapchain, nullptr);
    }, "Destroy swapchain");

    return info;
}

std::vector<VkImageView> CreateSwapchainImageViews(VkDevice device, std::vector<VkImage> const& swapchain_images,
                                                   VkFormat swapchain_format, uint32_t texture_mip_levels, DeletionStack& deletors)
{
    std::vector<VkImageView> swapchain_image_views;
    swapchain_image_views.resize(swapchain_images.size());
    for(uint32_t i = 0; i < swapchain_images.size(); i++)
    {
       if( CreateImageView(device, swapchain_images[i], swapchain_format, VK_IMAGE_ASPECT_COLOR_BIT,
                           swapchain_image_views[i], texture_mip_levels, deletors) != VK_SUCCESS)
          throw VulkanException("Cannot create image view!");

    }
    return swapchain_image_views;
}

VkRenderPass CreateRenderPass(VkDevice device, VkFormat swapchain_format, Image const& depth_info, VkSampleCountFlagBits /*msaa_samples*/)
{
    // Color attachment
    VkAttachmentDescription color_attachment = {};
    color_attachment.format = swapchain_format;
    color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    color_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    color_attachment.flags = 0;

    VkAttachmentReference attachment_ref = {};
    attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachment_ref.attachment = 0u;

    // Depth attachment
    VkAttachmentDescription depth_attachment = {};
    depth_attachment.format = depth_info.m_format;
    depth_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depth_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depth_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    depth_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depth_attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    VkAttachmentReference depth_attachment_ref = {};

    depth_attachment_ref.layout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL;
    depth_attachment_ref.attachment = 1u;

    VkSubpassDescription subpass_desc = {};
    subpass_desc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_desc.colorAttachmentCount = 1u;
    subpass_desc.pColorAttachments = &attachment_ref;
    subpass_desc.pDepthStencilAttachment = &depth_attachment_ref;

    std::array<VkAttachmentDescription, 2> attachments {color_attachment, depth_attachment};
    VkRenderPassCreateInfo render_pass_create_info = {};
    render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    render_pass_create_info.attachmentCount = static_cast<uint32_t>(attachments.size());
    render_pass_create_info.pAttachments = attachments.data();
    render_pass_create_info.subpassCount = 1;
    render_pass_create_info.pSubpasses = &subpass_desc;

    VkRenderPass render_pass;
    if(vkCreateRenderPass(device, &render_pass_create_info, nullptr, &render_pass) != VK_SUCCESS)
       throw VulkanException("Cannot create render pass");
    return render_pass;
}

std::vector<VkFramebuffer> CreateFramebuffers(VkDevice device, std::vector<VkImageView> const& image_views,
                                              VkRenderPass render_pass, VkExtent2D extent, VkImageView depth_image_view, DeletionStack& deletors)
{
    std::vector<VkFramebuffer> frame_buffers;
    frame_buffers.resize(image_views.size());
    for(uint32_t i = 0; i < image_views.size(); i++)
    {
       std::array<VkImageView, 2> attachments = { image_views[i], depth_image_view };
       VkFramebufferCreateInfo create_info = {};
       create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
       create_info.pNext = nullptr;
       create_info.attachmentCount = static_cast<uint32_t>(attachments.size());
       create_info.pAttachments = attachments.data();
       create_info.renderPass = render_pass;
       create_info.width = extent.width;
       create_info.height = extent.height;
       create_info.layers = 1;
       if(vkCreateFramebuffer(device, &create_info, nullptr, &frame_buffers[i]) != VK_SUCCESS)
          throw VulkanException("Cannot create frame buffer");
       deletors.Push([=](){
              vkDestroyFramebuffer(device, frame_buffers[i], nullptr);
       }, "Destroy Framebuffer");
    }
    return frame_buffers;
}
} // anonymous namespace

void VulkanSwapchain::Build(VulkanCore const& core, VmaAllocator alloc)
{
    m_info = CreateSwapchain(core.m_physical_device, core.m_device, core.m_surface, core.m_window, m_swapchain_images, m_deletors);
    m_swapchain_image_views = CreateSwapchainImageViews(core.m_device, m_swapchain_images, m_info.m_format, m_texture_mip_levels, m_deletors);
    m_depth_info.m_format = VK_FORMAT_D32_SFLOAT;
    m_depth_info.m_image_view_aspect_flags = VK_IMAGE_ASPECT_DEPTH_BIT;
    m_depth_info.m_flags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    m_depth_info.m_vma_usage = VMA_MEMORY_USAGE_GPU_ONLY;

    VkExtent3D extent = { m_info.m_extent.width, m_info.m_extent.height, 1 };
    m_depth_info.Create(core, m_deletors, alloc, extent);

    m_render_pass = CreateRenderPass(core.m_device, m_info.m_format, m_depth_info, core.m_msaa_samples);
    m_deletors.Push([=]()
    {
        vkDestroyRenderPass(core.m_device, m_render_pass, nullptr);
    }, "Destroy render pass");

    m_swapchain_frame_buffers = CreateFramebuffers(core.m_device, m_swapchain_image_views, m_render_pass,
                                                   m_info.m_extent, m_depth_info.m_image_view, m_deletors);
}

void VulkanSwapchain::Shutdown()
{
    m_deletors.Flush();
}
} // namespace hs::graphics::vulkan
