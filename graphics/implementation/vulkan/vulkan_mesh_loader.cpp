#include "vulkan_mesh_loader.hpp"
#include "vulkan_utils.hpp"
#include <sstream>
#include <thread>

namespace hs::graphics::vulkan
{
void MeshLoader::Load(VkDevice device, VkQueue graphics_queue, VmaAllocator allocator, DeletionStack& deletors,
                      VulkanMesh& v_mesh, VulkanCommands& commands)
{
    VkBufferCreateInfo info = {};
    auto mesh = v_mesh.GetMesh();
    auto buf_size = mesh.vertices.size() * sizeof(Vertex);
    info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    info.size = buf_size;
    std::stringstream ss;
    ss << "Mesh size is " << mesh.vertices.size() * sizeof(Vertex) << ". " << std::endl;
    DebugMessage(ss.str());
    info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    VmaAllocationCreateInfo allocation_info = {};
    allocation_info.usage = VMA_MEMORY_USAGE_CPU_ONLY;
    AllocatedBuffer staging_buffer;

    if(vmaCreateBuffer(allocator, &info, &allocation_info,
                       &staging_buffer.m_buffer, &staging_buffer.m_allocation,
                       nullptr) != VK_SUCCESS)
        throw VulkanException("Failed to allocate mesh staging buffer");

    void* vertices_data;
    vmaMapMemory(allocator, staging_buffer.m_allocation, &vertices_data);
    memcpy(vertices_data, mesh.vertices.data(), mesh.vertices.size() * sizeof(Vertex));
    vmaUnmapMemory(allocator, staging_buffer.m_allocation);

    VkBufferCreateInfo gpu_buffer_info = {};
    gpu_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    gpu_buffer_info.pNext = nullptr;
    gpu_buffer_info.size = buf_size;
    gpu_buffer_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    allocation_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    if(vmaCreateBuffer(allocator, &gpu_buffer_info, &allocation_info,
                           &v_mesh.m_allocated_buffer.m_buffer, &v_mesh.m_allocated_buffer.m_allocation,
                           nullptr) != VK_SUCCESS)
            throw VulkanException("Failed to allocate mesh staging buffer");

    commands.ImmediateSubmit(device, graphics_queue, [=](VkCommandBuffer cmd)
    {
        VkBufferCopy copy;
        copy.dstOffset = 0;
        copy.srcOffset = 0;
        copy.size = buf_size;
        vkCmdCopyBuffer(cmd, staging_buffer.m_buffer, v_mesh.m_allocated_buffer.m_buffer, 1, &copy);
    });

    deletors.Push([=]()
    {
        vmaDestroyBuffer(allocator, v_mesh.m_allocated_buffer.m_buffer, v_mesh.m_allocated_buffer.m_allocation);
    }, "Destroy mesh buffer");
    vmaDestroyBuffer(allocator, staging_buffer.m_buffer, staging_buffer.m_allocation);
}
} // namespace hs::graphics::vulkan
