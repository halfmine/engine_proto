#pragma once
#include "vulkan_utils.hpp"
#include "vulkan_commands.hpp"

namespace hs::graphics::vulkan
{
struct MeshLoader
{
    void Load(VkDevice device, VkQueue graphics_queue, VmaAllocator allocator, DeletionStack& deletors,
              VulkanMesh& v_mesh, VulkanCommands& commands);
};
} // namespace hs::graphics::vulkan
