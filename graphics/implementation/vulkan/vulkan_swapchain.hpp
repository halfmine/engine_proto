#pragma once
#include "vulkan_utils.hpp"
#include "vulkan_core.hpp"
#include "vulkan_image_builder.hpp"
#include <vulkan/vulkan.hpp>
#include <vector>

namespace hs::graphics::vulkan
{
struct SwapchainInfo
{
    VkSwapchainKHR m_swapchain;
    VkExtent2D m_extent;
    VkFormat m_format;
};

class VulkanSwapchain
{
public:
    VulkanSwapchain() = default;
    ~VulkanSwapchain() = default;

    void Build(VulkanCore const& core, VmaAllocator alloc);
    void Shutdown();

    SwapchainInfo m_info;
    Image m_depth_info;

    std::vector<VkImage> m_swapchain_images;
    std::vector<VkImageView> m_swapchain_image_views;

    std::vector<VkFramebuffer> m_swapchain_frame_buffers;

    VkRenderPass m_render_pass;

    uint32_t m_texture_mip_levels;
    DeletionStack m_deletors;
};
} // namespace hs::graphics::vulkan
