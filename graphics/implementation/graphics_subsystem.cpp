#include "graphics_subsystem.hpp"
#include "vulkan_renderer.hpp"
#include "app_exception.hpp"
#include "app_layer.hpp"
#include "scene_resource.hpp"
#include <memory>

namespace hs::graphics
{
class GraphicsException : public app_layer::AppException
{
public:
   GraphicsException(const std::string msg) : app_layer::AppException(std::move(msg))
   {
   }

   GraphicsException(const std::string_view msg) : app_layer::AppException(msg.data())
   {
   }

   GraphicsException(const char* msg_chars) : app_layer::AppException(msg_chars)
   {
   }
};

class GraphicsSubsystem::Impl
{
public:
    Impl(void* window, std::string scene_path, GraphicsEngineType type)
        : m_renderer(nullptr), m_scene_path(std::move(scene_path)), m_scene_resource_name("Scene"), m_window(window),
          m_type(type)
    {
    }

    ~Impl() = default;

    void Init()
    {
        switch(m_type)
        {
        case GraphicsEngineType::Vulkan:
           m_renderer = std::make_unique<vulkan::VulkanRenderer>(m_window);
           break;
        default:
           throw GraphicsException("Not supported yet.");
        }
        auto& input = app_layer::AppLayer::Instance().GetInput();
        input.AddListener(m_renderer->InputHandler(), m_renderer->InputReleasedHandler());
        auto scene_resource_ptr = std::make_unique<SceneResource>(m_scene_path);
        scene_resource_ptr->Load();
        resources::ResourceManager::Instance().RegisterResource(std::move(scene_resource_ptr));
        m_renderer->Init();
    }
    
    void Run()
    {
        m_renderer->Run();
    }
    
    void Shutdown()
    {
        m_renderer->Shutdown();
    }
private:
    std::unique_ptr<IRenderer> m_renderer;
    std::string m_scene_path;
    std::string m_scene_resource_name;
    void* m_window;
    GraphicsEngineType m_type;
};
GraphicsSubsystem::GraphicsSubsystem(const GraphicsEngineType type, void* window, std::string scene_path)
    : m_impl(std::make_unique<GraphicsSubsystem::Impl>(window, scene_path, type))
{
}

void GraphicsSubsystem::Init()
{
    m_impl->Init();
}

void GraphicsSubsystem::Run()
{
    m_impl->Run();
}

void GraphicsSubsystem::Shutdown()
{
    m_impl->Shutdown();
}

GraphicsSubsystem::~GraphicsSubsystem() = default;
} // namespace hs::graphics
