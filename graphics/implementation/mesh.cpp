#include "mesh.hpp"

#include "app_exception.hpp"
#include <tiny_obj_loader.h>
#include <unordered_map>
#include <sstream>
#include <string>
#include <iostream>
#include <memory>

namespace hs::graphics
{
using namespace std::literals::string_view_literals;

Mesh MeshComponent::GetMesh() const
{
    auto resource_ptr = Resource();
    if(resource_ptr)
        return std::dynamic_pointer_cast<MeshResource>(resource_ptr)->GetMesh();
    throw resources::ResourceException("Failed to load mesh resource");
}

void MeshResource::Load()
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if(!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, m_filename.c_str()))
    {
       std::stringstream ss;
       ss << "Failed to load model: " << warn + err << std::endl;
       throw app_layer::AppException(ss.str().c_str());
    }

    for (size_t s = 0; s < shapes.size(); s++)
    {
            // Loop over faces(polygon)
            size_t index_offset = 0;
            for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
            {

                //hardcode loading to triangles
                auto fv = 3u;

                // Loop over vertices in the face.
                for (size_t v = 0; v < fv; v++)
                {
                    // access to vertex
                    tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

                    //vertex position
                    tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
                    tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
                    tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
                    //vertex normal
                    tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
                    tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
                    tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];

                    tinyobj::real_t uv_x = attrib.texcoords[ 2 * idx.texcoord_index + 0 ];
                    tinyobj::real_t uv_y = attrib.texcoords[ 2 * idx.texcoord_index + 1 ];

                    //copy it into our vertex
                    Vertex new_vert;
                    new_vert.position.x = vx;
                    new_vert.position.y = vy;
                    new_vert.position.z = vz;

                    new_vert.normal.x = nx;
                    new_vert.normal.y = ny;
                    new_vert.normal.z = nz;

                    new_vert.uv.x = uv_x;
                    new_vert.uv.y = 1 - uv_y;

                    //we are setting the vertex color as the vertex normal. This is just for display purposes
                    new_vert.color = new_vert.normal;



                    m_mesh.vertices.push_back(new_vert);
                }
                index_offset += fv;
            }
        }
    m_loaded = true;
}

const char* MeshResource::RawResource(size_t &out_resource_size)
{
    out_resource_size = 0;
    return "";
}

std::string_view MeshResource::Name()
{
    return m_name;
}
}
