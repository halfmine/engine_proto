#version 460

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec3 vColor;

layout (location = 0) out vec3 outColor;

layout(set = 0, binding = 0) uniform CameraBuffer
{
    mat4 view;
    mat4 proj;
    mat4 viewproj;
} camera_data;

struct ObjectData
{
    mat4 model;
};

layout(std140, set = 1, binding = 0) readonly buffer ObjectBuffer
{
    ObjectData objects[];
} objects_buffer;

void main()
{
    mat4 model_matrix = objects_buffer.objects[gl_BaseInstance].model;
    mat4 transfor_matrix = (camera_data.viewproj * model_matrix);
    gl_Position = transfor_matrix * vec4(vPosition, 1.0f);
    outColor = vColor;
}
