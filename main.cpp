#include "app_layer.hpp"
#include "core/include/app_exception.hpp"
#include <cmdlime/config.h>
#include <iostream>

using namespace hs;

struct HSConfig : public cmdlime::Config
{
    PARAM(width, uint16_t)(800);
    PARAM(height, uint16_t)(600);
    PARAM(msaa_count, size_t)(0);
    ARG(scene_path, std::string);
};

int main(int argc, char** argv)
{
    HSConfig cfg;
    auto reader = cmdlime::ConfigReader{cfg, "engine_proto"};
    if(!reader.readCommandLine(argc, argv))
        return reader.exitCode();

    app_layer::AppLayer& app = app_layer::AppLayer::Instance();
    // calling engine entrypoint
    app_layer::AppOptions opts;
    opts.screen_height = cfg.height;
    opts.screen_width = cfg.width;
    opts.msaa_count = cfg.msaa_count;
    opts.window_title = "HS Engine";
    opts.scene_path = cfg.scene_path;
    int code = 0;
    try
    {
        app.Init(opts);
        app.Run();
        app.Shutdown();
    }
    catch(const app_layer::AppException& e)
    {
        std::cerr << e.what() << std::endl;
        code = -1;
    }
    catch(...)
    {
        code = -2;
    }

    return code;
}
