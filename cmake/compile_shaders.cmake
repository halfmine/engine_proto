macro(hs_compile_vertex_shader NAME FILENAME)
add_custom_command(COMMENT "Compiling vertex shader ${FILENAME}"
                   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv
                   COMMAND ${HS_GLSLC_EXECUTABLE} -o ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv ${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME}
                   MAIN_DEPENDENCY ${FILENAME})
add_custom_target(${NAME}_vertex DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv)
add_dependencies(${target} ${NAME}_vertex)
endmacro()

macro( hs_compile_fragment_shader NAME FILENAME)
add_custom_command(COMMENT "Compiling fragment shader ${FILENAME}"
                   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv
                   COMMAND ${HS_GLSLC_EXECUTABLE} -o ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv ${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME}
                   MAIN_DEPENDENCY ${FILENAME})
add_custom_target(${NAME}_fragment DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}.spv)
add_dependencies(${target} ${NAME}_fragment)
endmacro()
